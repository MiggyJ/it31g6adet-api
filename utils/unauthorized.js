const unauthorized = (err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
      res.status(401).send({
        error: true,
        message: 'You need to log in first.'
      });
    }
}

module.exports = unauthorized

