const staffMiddleWare = (req, res, next) => {
    if (req.user.role.name != 'Staff') {
        res.sendStatus(401)
    } else {
        next()
    }
}

module.exports = staffMiddleWare