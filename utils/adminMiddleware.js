const adminMiddleware = (req, res, next) => {
    if (req.user.role.name != 'Admin') {
        res.sendStatus(401)
    } else {
        next()
    }
}

module.exports = adminMiddleware