'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Officials', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      resident_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'residents'
          },
          key: 'id'
        }
      },
      position: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      term_start: {
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      term_end: {
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(async () => {
      await queryInterface.addConstraint('officials', {
        fields: ['resident_id', 'position', 'term_start', 'term_end'],
        type: 'unique',
        name: 'UQ_OFFICIAL_TERM'
      })
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Officials');
  }
};