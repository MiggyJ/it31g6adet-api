'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Witnesses', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      summon_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'summons'
          },
          key: 'id'
        }
      },
      resident_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'residents'
          },
          key: 'id'
        }
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(async () => {
      await queryInterface.addConstraint('witnesses', {
        fields: [
          'summon_id',
          'resident_id',
        ],
        type: 'unique',
        name: 'UQ_SUMMON_WITNESS'
      })
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Witnesses');
  }
};