'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Relief_Recipients', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      operation_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'relief_operations'
          },
          key: 'id'
        }
      },
      recipient_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'residents'
          },
          key: 'id'
        }
      },
      item_received: {
        type: Sequelize.STRING,
        allowNull: false
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(async () => {
      await queryInterface.addConstraint('Relief_Recipients', {
        fields: ['operation_id', 'recipient_id'],
        type: 'unique',
        name: 'UQ_OPERATION_RECIPIENT'
      })
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Relief_Recipients');
  }
};