'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      resident_id: {
        type: Sequelize.UUID,
        unique: true,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false
      },
      role_id: {
        type: Sequelize.UUID,
        allowNull: false,
        defaultValue: 1,
        references: {
          model: {
            tableName: 'roles'
          },
          key: 'id'
        }
      },
      status: {
        type: Sequelize.STRING,
        defaultValue: 'Active'
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(async () => {
      await queryInterface.addConstraint('roles', {
        fields: ['created_by'],
        type: 'foreign key',
        name: 'FK_USER_ROLE_CREATE',
        references: {
          table: 'users',
          field: 'id'
        }
      })
      await queryInterface.addConstraint('roles', {
        fields: ['updated_by'],
        type: 'foreign key',
        name: 'FK_USER_ROLE_UPDATE',
        references: {
          table: 'users',
          field: 'id'
        }
      })
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('roles', 'FK_USER_ROLE_CREATE')
    await queryInterface.removeConstraint('roles', 'FK_USER_ROLE_UPDATE')
    await queryInterface.dropTable('Users');
  }
};