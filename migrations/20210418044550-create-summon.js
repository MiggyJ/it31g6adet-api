'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Summons', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      complaint_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'complaints'
          },
          key: 'id'
        }
      },
      summon_number: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      summon_date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      summon_time: {
        type: Sequelize.TIME,
        allowNull: false
      },
      duration: {
        type: Sequelize.INTEGER,
      },
      mediator_id: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'officials'
          },
          key: 'id'
        }
      },
      details: {
        type: Sequelize.TEXT,
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      created_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        }
      },
      updated_by: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'users'
          },
          key: 'id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Summons');
  }
};