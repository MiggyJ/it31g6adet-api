'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Residents', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      first_name: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      middle_name: {
        type: Sequelize.STRING(50)
      },
      last_name: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      gender: {
        type: Sequelize.STRING,
        allowNull: false
      },
      civil_status: {
        type: Sequelize.STRING,
        allowNull: false
      },
      birth_date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      permanent_house_number: {
        type: Sequelize.STRING(5),
      },
      permanent_street: {
        type: Sequelize.STRING(50),
      },
      permanent_barangay: {
        type: Sequelize.STRING,
      },
      permanent_municipality: {
        type: Sequelize.STRING,
      },
      permanent_province: {
        type: Sequelize.STRING,
      },
      current_house_number: {
        type: Sequelize.STRING,
        allowNull: false
      },
      current_street: {
        type: Sequelize.STRING,
        allowNull: false
      },
      current_barangay: {
        type: Sequelize.STRING,
        allowNull: false
      },
      current_municipality: {
        type: Sequelize.STRING,
        allowNull: false
      },
      current_province: {
        type: Sequelize.STRING,
        allowNull: false
      },
      personal_contact_number: {
        type: Sequelize.STRING,
        allowNull: false
      },
      benefits: {
        type: Sequelize.STRING,
      },
      emergency_contact_person_1: {
        type: Sequelize.STRING,
      },
      emergency_contact_number_1: {
        type: Sequelize.STRING,
      },
      emergency_contact_person_2: {
        type: Sequelize.STRING,
      },
      emergency_contact_number_2: {
        type: Sequelize.STRING,
      },
      citizenship: {
        type: Sequelize.STRING,
      },
      nationality: {
        type: Sequelize.STRING,
      },
      blood_type: {
        type: Sequelize.STRING,
      },
      occupation: {
        type: Sequelize.STRING
      },
      height: {
        type: Sequelize.INTEGER
      },
      weight: {
        type: Sequelize.INTEGER
      },
      hair_color: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(async () => {
      await queryInterface.addConstraint('users', {
        fields: ['resident_id'],
        type: 'foreign key',
        name: 'FK_USER_RESIDENT',
        references: {
          table: 'residents',
          field: 'id'
        }
      })
    }).then(async () => {
      await queryInterface.addConstraint('ordinances', {
        fields: ['author'],
        type: 'foreign key',
        name: 'FK_ORDINANCE_AUTHOR',
        references: {
          table: 'residents',
          field: 'id'
        }
      })
    }).then(async () => {
      await queryInterface.addConstraint('residents', {
        fields: [
          'first_name',
          'birth_date',
          'last_name',
          'current_house_number',
          'current_street'
        ],
        type: 'unique',
        name: 'UQ_RESIDENT_PROFILE'
      })
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint('users', 'FK_USER_RESIDENT')
    await queryInterface.removeConstraint('ordinances', 'FK_ORDINANCE_AUTHOR')
    await queryInterface.dropTable('Residents');
  }
};