'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Blotters', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      blotter_number: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      complainant_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'residents'
          },
          key: 'id'
        }
      },
      defendant_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'residents'
          },
          key: 'id'
        }
      },
      reason: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      details: {
        type: Sequelize.TEXT,
        allowNull: false,
      },
      incidence_place: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      incidence_date: {
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      officer_id: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: {
            tableName: 'officials'
          },
          key: 'id'
        }
      },
      status: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Blotters');
  }
};