'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Officials', [
      {
        id: 'bb7b7049-a305-488b-b4f2-ce4496ac4803',
        resident_id: '8785817a-1aa0-4767-b51e-1f07cde9540e',
        position: 'Barangay Chairman',
        term_start: new Date('2021-01-01'),
        term_end: new Date('2022-12-31'),
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '88f2596a-c56d-4c06-92c6-156be80b7e33',
        resident_id: '3024e69e-bf45-404d-b9d6-bace74fa80bc',
        position: 'Barangay Councilor',
        term_start: new Date('2021-01-01'),
        term_end: new Date('2022-12-31'),
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '31591a1e-1cdf-491d-9046-c8f048fd76ed',
        resident_id: 'cc552f92-b352-4f5d-9cfb-163fae500285c',
        position: 'Barangay Councilor',
        term_start: new Date('2021-01-01'),
        term_end: new Date('2022-12-31'),
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '9b664105-8d78-4ba4-bbc8-a427493b71a8',
        resident_id: 'fd5d9aca-45f4-49b5-992f-b603e0cb7b4e',
        position: 'Barangay Councilor',
        term_start: new Date('2021-01-01'),
        term_end: new Date('2022-12-31'),
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '1f2a048a-6437-47b9-bf50-92a3915b4194',
        resident_id: 'c1d7f036-1ee7-487c-8615-08335f3ea79b',
        position: 'Barangay Councilor',
        term_start: new Date('2021-01-01'),
        term_end: new Date('2022-12-31'),
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '0236a944-f447-4f36-9905-759e7b537a51',
        resident_id: '2e915dc7-ef9b-4a42-ad17-702765dd7b8e',
        position: 'Barangay Councilor',
        term_start: new Date('2021-01-01'),
        term_end: new Date('2022-12-31'),
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 'b560aa6f-58ff-4a4f-ac54-4c336a935031',
        resident_id: '423a6af8-7396-41a3-98d3-e133da71c784',
        position: 'Barangay Councilor',
        term_start: new Date('2021-01-01'),
        term_end: new Date('2022-12-31'),
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '789e61dd-15d6-444a-ada0-0a8101e5b98f',
        resident_id: 'c5d93987-8334-4ca7-8065-9dc9e1a96124',
        position: 'Barangay Councilor',
        term_start: new Date('2021-01-01'),
        term_end: new Date('2022-12-31'),
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 'ae68c63f-fe16-4389-bc3b-8371baa9b8ea',
        resident_id: '3aca960f-438d-483d-a2a6-9bb3b1444ec9',
        position: 'SK Chairman',
        term_start: new Date('2021-01-01'),
        term_end: new Date('2022-12-31'),
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Officials', null, {});
  }
};
