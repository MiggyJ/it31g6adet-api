'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Residents', [
      {
        id: 'a33a5622-b28b-4d21-b7d4-4087599b6c5d',
        first_name: 'Jake',
        last_name: 'Lee',
        gender: 'Male',
        civil_status: 'Single',
        birth_date: new Date('1966-06-04'),
        personal_contact_number: '9457238479',
        benefits: '4Ps;Indigent;Senior Citizen',
        current_house_number: '10',
        current_street: 'Doon Street',
        current_barangay: 'Hehi 2',
        current_municipality: 'Ila',
        current_province: 'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '47d25798-fb4a-45dc-9f15-abd2532ffa8c',
        first_name: 'Gina',
        last_name: 'Kreger',
        gender: 'Female',
        civil_status: 'Single',
        birth_date: new Date('1989-10-14'),
        personal_contact_number: '9227988474',
        benefits: '4Ps',
        current_house_number: '3',
        current_street: 'Dian Street',
        current_barangay: 'Hehi 2',
        current_municipality: 'Ila',
        current_province: 'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '239b3df9-44e8-4f1e-ac21-b515522ed27f',
        first_name: 'Rover',
        last_name: 'Gil',
        gender: 'Male',
        civil_status: 'Single',
        birth_date: new Date('1991-08-24'),
        personal_contact_number: '9113948372',
        benefits: '',
        current_house_number: '3',
        current_street: 'Pitasan Street',
        current_barangay: 'Hehi 2',
        current_municipality: 'Ila',
        current_province: 'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '8785817a-1aa0-4767-b51e-1f07cde9540e',
        first_name: 'Jerald',
        last_name: 'Simon',
        gender: 'Male',
        civil_status: 'Single',
        birth_date: new Date('1985-02-07'),
        personal_contact_number: '9385586940',
        benefits: '',
        current_house_number: '17',
        current_street: 'Looban Street',
        current_barangay: 'Hehi 2',
        current_municipality: 'Ila',
        current_province: 'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '3024e69e-bf45-404d-b9d6-bace74fa80bc',
        first_name:'Lance',
        last_name:'Jose',
        gender:'Male',
        civil_status:'Single',
        birth_date: new Date('1993-02-24'),
        personal_contact_number:'9468586769',
        benefits:'',
        current_house_number:'45',
        current_street:'First Street',
        current_barangay:'Hehi 2',
        current_municipality:'Ila',
        current_province:'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 'cc552f92-b352-4f5d-9cfb-163fae500285c',
        first_name:'Karl Jayce',
        last_name:'Stewart',
        gender:'Male',
        civil_status:'Single',
        birth_date: new Date('1973-09-30'),
        personal_contact_number:'9236564659',
        benefits:'',
        current_house_number:'69',
        current_street:'First Street',
        current_barangay:'Hehi 2',
        current_municipality:'Ila',
        current_province:'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 'fd5d9aca-45f4-49b5-992f-b603e0cb7b4e',
        first_name:'Layla',
        last_name:'Cruz',
        gender:'Female',
        civil_status:'Married',
        birth_date: new Date('1984-03-24'),
        personal_contact_number:'9287542591',
        benefits:'',
        current_house_number:'58',
        current_street:'Daisy Street',
        current_barangay:'Hehi 2',
        current_municipality:'Ila',
        current_province:'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 'c1d7f036-1ee7-487c-8615-08335f3ea79b',
        first_name:'Joseph',
        last_name:'Cruz',
        gender:'Male',
        civil_status:'Married',
        birth_date: new Date('1986-07-05'),
        personal_contact_number:'9199572387',
        benefits:'',
        current_house_number:'58',
        current_street:'Daisy Street',
        current_barangay:'Hehi 2',
        current_municipality:'Ila',
        current_province:'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 'c5d93987-8334-4ca7-8065-9dc9e1a96124',
        first_name:'Felix',
        last_name:'Miguel',
        gender:'Male',
        civil_status:'Single',
        birth_date: new Date('1995-12-20'),
        personal_contact_number:'9199059933',
        benefits:'',
        current_house_number:'3',
        current_street:'Bugatti Street',
        current_barangay:'Hehi 2',
        current_municipality:'Ila',
        current_province:'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '2e915dc7-ef9b-4a42-ad17-702765dd7b8e',
        first_name:'Elaine',
        last_name:'Perez',
        gender:'Female',
        civil_status:'Single',
        birth_date: new Date('1997-09-17'),
        personal_contact_number:'9195557708',
        benefits:'',
        current_house_number:'10',
        current_street:'Senegal Street',
        current_barangay:'Hehi 2',
        current_municipality:'Ila',
        current_province:'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '64bf1148-37f3-415e-80f2-6d400c1f458a',
        first_name:'Joselita',
        last_name:'Ramos',
        gender:'Female',
        civil_status:'Married',
        birth_date: new Date('1963-08-16'),
        personal_contact_number:'9805150291',
        benefits:'',
        current_house_number:'24',
        current_street:'Rosal Street',
        current_barangay:'Hehi 2',
        current_municipality:'Ila',
        current_province:'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '423a6af8-7396-41a3-98d3-e133da71c784',
        first_name:'Consuelo',
        last_name:'Ramos',
        gender:'Male',
        civil_status:'Married',
        birth_date: new Date('1969-10-14'),
        personal_contact_number:'9338353686',
        benefits:'',
        current_house_number:'24',
        current_street:'Rosal Street',
        current_barangay:'Hehi 2',
        current_municipality:'Ila',
        current_province:'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '3aca960f-438d-483d-a2a6-9bb3b1444ec9',
        first_name:'Marco',
        last_name:'Lovino',
        gender:'Male',
        civil_status:'Single',
        birth_date: new Date('1999-05-13'),
        personal_contact_number:'93253959608',
        benefits:'',
        current_house_number:'57',
        current_street:'Second Street',
        current_barangay:'Hehi 2',
        current_municipality:'Ila',
        current_province:'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '994384c5-208b-4f43-bfe2-e74efad380f5',
        first_name:'Kailh',
        last_name:'So',
        gender:'Male',
        civil_status:'Single',
        birth_date: new Date('2001-09-01'),
        personal_contact_number:'9335931271',
        benefits:'',
        current_house_number:'42',
        current_street:'Senegal Street',
        current_barangay:'Hehi 2',
        current_municipality:'Ila',
        current_province:'Region 18',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
        
      },
      
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Residents', null, {});

  }
};
