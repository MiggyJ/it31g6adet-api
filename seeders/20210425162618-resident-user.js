'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Users', [
      {
        id: '316eee62-9ac3-4cf4-a44d-d1d6fd71fa2e',
        resident_id: '8785817a-1aa0-4767-b51e-1f07cde9540e',
        email: 'admin@thissite.com',
        password: '$2b$10$EFphlLlG82zcQtHRjIkvRex3yIB4pYq77GREJcq5VF3reEgX.fJpG',
        role_id: '8b36fc44-a338-4cf2-995e-4d4375c1369c',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '29e9f490-f701-4426-88d2-1a5533d99476',
        resident_id: 'a33a5622-b28b-4d21-b7d4-4087599b6c5d',
        email: 'jakelee23@gmail.com',
        password: '$2b$10$EFphlLlG82zcQtHRjIkvRex3yIB4pYq77GREJcq5VF3reEgX.fJpG',
        role_id: '07eb9503-b297-4613-8939-39cd9b573cbb',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        resident_id: '994384c5-208b-4f43-bfe2-e74efad380f5',
        email: 'staff1@thissite.com',
        password: '$2b$10$EFphlLlG82zcQtHRjIkvRex3yIB4pYq77GREJcq5VF3reEgX.fJpG',
        role_id: '482fc1ad-0973-4f0f-9746-0a5591677296',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
       },
       {
        id: '34ac0116-9099-4c0d-bf61-274ee65dc41b',
        resident_id: '3024e69e-bf45-404d-b9d6-bace74fa80bc',
        email: 'lancejose@gmail.com',
        password: '$2b$10$EFphlLlG82zcQtHRjIkvRex3yIB4pYq77GREJcq5VF3reEgX.fJpG',
        role_id: '07eb9503-b297-4613-8939-39cd9b573cbb',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
       },
       {
        id: 'ab3f0298-2876-4c82-9450-c1e71c19a2a8',
        resident_id: 'c1d7f036-1ee7-487c-8615-08335f3ea79b',
        email: 'josephcruz@gmail.com',
        password: '$2b$10$EFphlLlG82zcQtHRjIkvRex3yIB4pYq77GREJcq5VF3reEgX.fJpG',
        role_id: '07eb9503-b297-4613-8939-39cd9b573cbb',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
       }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Users', null, {})
  }
};
