'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Certificates',
      [
        {
          id: '16e5c8ed-33c5-480a-92d4-f8a36d932164',
          certificate_number: new Date().getFullYear() + '-A10001',
          type: 'Barangay',
          owner_id: 'c1d7f036-1ee7-487c-8615-08335f3ea79b',
          status: 'Request',
          created_at: new Date(),
          updated_at: new Date()
        },
        {
          id: '16e5c8ed8984799b-2983-43fe-8660-51a912feadc0',
          certificate_number: new Date().getFullYear() + '-D10001',
          type: 'Indigency',
          owner_id: 'c1d7f036-1ee7-487c-8615-08335f3ea79b',
          status: 'Request',
          created_at: new Date(),
          updated_at: new Date()
        }
      ]
    )
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Certificates', null, {})
  }
};
