'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('Complaints', [
      {
        id: '3c11f9b6-970e-4d79-a4a8-6e435904b53e',
        complaint_number: `COMPLAINT-${new Date().getFullYear()}_101`,
        complainant_id: '3024e69e-bf45-404d-b9d6-bace74fa80bc',
        defendant_id: 'cc552f92-b352-4f5d-9cfb-163fae500285c',
        complaint_date: '01-10-21',
        details: 'chismis dito. chismis doon',
        status: 'Unresolved',
        isActive: true,
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 'f34f7e38-b0ce-4be1-936d-1a94e5b87270',
        complaint_number: `COMPLAINT-${new Date().getFullYear()}_102`,
        complainant_id: '3024e69e-bf45-404d-b9d6-bace74fa80bc',
        defendant_id: 'cc552f92-b352-4f5d-9cfb-163fae500285c',
        complaint_date: '01-12-21',
        details: 'sinaksak ng ballpen.',
        status: 'Unresolved',
        isActive: true,
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: 'e2cc4334-577f-4796-bf41-dbdaba7a94f5',
        complaint_number: `COMPLAINT-${new Date().getFullYear()}_103`,
        complainant_id: 'cc552f92-b352-4f5d-9cfb-163fae500285c',
        defendant_id: '3024e69e-bf45-404d-b9d6-bace74fa80bc',
        complaint_date: '01-11-21',
        details: 'Reklamador ng taon.',
        status: 'Unresolved',
        isActive: true,
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        created_at: new Date(),
        updated_at: new Date()
      },
    ], {})

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Complaints', null, {});
  }
};
