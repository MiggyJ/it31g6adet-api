'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Blotters', [
      {
        id: '05c42df0-db8d-4820-96da-eed8db722054',
        blotter_number: '2021-01',
        complainant_id: '3024e69e-bf45-404d-b9d6-bace74fa80bc',
        defendant_id: 'cc552f92-b352-4f5d-9cfb-163fae500285c',
        reason: 'sinaksak sa pwet',
        details: 'tinurbo sa pwet kaya dumugo',
        incidence_place: 'bahay ni aling cely',
        incidence_date: new Date('01-01-21'),
        officer_id:'31591a1e-1cdf-491d-9046-c8f048fd76ed',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '3686e981-fc15-4c95-b501-7be03cf964c1',
        blotter_number: '2021-02',
        complainant_id: 'cc552f92-b352-4f5d-9cfb-163fae500285c',
        defendant_id: '3024e69e-bf45-404d-b9d6-bace74fa80bc',
        reason: 'nilason ang aso ng kapitbahay',
        details: 'binawian yung sumaksak sa pwet',
        incidence_place: 'plaza',
        incidence_date: new Date('01-05-21'),
        officer_id:'31591a1e-1cdf-491d-9046-c8f048fd76ed',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '82d285b9-78e3-4197-863b-16fd68f48b71',
        blotter_number: '2021-03',
        complainant_id: '423a6af8-7396-41a3-98d3-e133da71c784',
        defendant_id: '3aca960f-438d-483d-a2a6-9bb3b1444ec9',
        reason: 'hindi tumulong sa system',
        details: 'hinampas ng keyboard sa mukha duguan.',
        incidence_place: 'computer lab sa PUPQC',
        incidence_date: new Date('03-29-20'),
        officer_id:'31591a1e-1cdf-491d-9046-c8f048fd76ed',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Blotters', null, {});
  }
};
