'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Announcements', [
      {
        id: '32ae8dd5-e6bc-45c1-98f7-a221eb6bcebf',
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        title: 'Barangay Assembly Day',
        body: "HELLO KABARANGUYS! Inaanyayahan po namin ang lahat na makiisa sa gaganaping State of the Barangay Address (SOBA) ni Kapitan Jojo M. Abad bukas, ika-17 ng Abril, Sabado sa ganap alas-8 ng umaga.",
        status: 'Active',
        file: 'placeholder.png',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '2235360a-cee0-4fcf-ac0f-48f1460589cb',
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        title: 'DSWD SAP ANNOUNCEMENT',
        body: "Narito ang kumpletong Masterlist ng mga pangalang kwalipikado para sa pamamahagi ng Ayuda. Ang listahang ito ay galing sa Quezon City. ",
        status: 'Active',
        file: 'placeholder.png',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 'ef3aa29b-132a-493b-adce-c5922778d012',
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        title: 'DSWD SAP DISTRIBUTION',
        body: "Magandang Araw Kabarangay! Narito ang mga Schedule ng DSWD SAP DISTRIBUTION. Mangyari lamang na pumunta sa araw na itinakda sa inyong Cluster. Basahin at unawain ang mga alituntunin na nasa larawan",
        status: 'Active',
        file: 'placeholder.png',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '269df9ef-900c-4148-93db-0fd3dd2a57a8',
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        title: 'New Barangay Hall',
        body: "Mapagpalang Araw! Simula sa abril 08, 2022 araw ng huwebes ay maaari ng kumuha ng Brgy. certificate/clearance, Brgy. Permits at cedula sa bagong barangay hall.",
        status: 'Active',
        file: 'placeholder.png',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '58ef904e-8f76-4ac3-80eb-ddcffbe24531',
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        title: 'COVID-19 VACCINE',
        body: "HELLO KABARANGUYS! Panawagan po sa lahat ng nasa listahan, kayo po ay naka-iskedyul mabigyan ng COVID-19 vaccine, ito ay gaganapin sa Linggo(ika-4 ng Abril) o sa Lunes(ika-5 ng Abril) mula 8:00 ng umaga hanggang 12:00 ng tanghali",
        status: 'Active',
        file: 'placeholder.png',
        created_at: new Date(),
        updated_at: new Date(),
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Announcements', null, {});
  }
};
