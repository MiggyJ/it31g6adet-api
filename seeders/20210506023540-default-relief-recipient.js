'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Relief_Recipients', [
      {
        id: 'e7bf5f59-d673-47e8-8bfb-90d25ad25f38',
        operation_id: '7b7d07ef-5699-4f7e-b73e-c290c2241f0f',
        recipient_id: 'a33a5622-b28b-4d21-b7d4-4087599b6c5d',
        item_received: 'Canned goods, utensils, bottled water, clothes, hygiene kits, and sleeping necessities.',
        status: 'Inactive',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 'adef1b0c-e03d-46dc-8b5f-d0a235ce5f50',
        operation_id: '08d7c4e5-7c4d-407c-bfc2-15ea8fee955b',
        recipient_id: 'fd5d9aca-45f4-49b5-992f-b603e0cb7b4e',
        item_received: 'Canned goods, utensils, bottled water, clothes, hygiene kits, and sleeping necessities.',
        status: 'Inactive',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '0a62238c-95d1-48eb-b9f1-7449d00aea59',
        operation_id: '0a30c27f-5ad5-43d2-98d1-b02cfd40b586',
        recipient_id: '47d25798-fb4a-45dc-9f15-abd2532ffa8c',
        item_received: 'Canned goods, utensils, bottled water, clothes, hygiene kits, and sleeping necessities.',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 'fa041ae4-918f-4506-905b-2394ec5e137a',
        operation_id: '0a30c27f-5ad5-43d2-98d1-b02cfd40b586',
        recipient_id: '239b3df9-44e8-4f1e-ac21-b515522ed27f',
        item_received: 'Canned goods, utensils, bottled water, clothes, hygiene kits, and sleeping necessities.',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '5cf32209-6629-4db4-9a9a-925b99400bd3',
        operation_id: '14391d4a-a52f-4b7d-bb1a-a4c4aa084fc5',
        recipient_id: '8785817a-1aa0-4767-b51e-1f07cde9540e',
        item_received: 'Canned goods, utensils, bottled water, clothes, hygiene kits, and sleeping necessities.',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '0719b33d-6d4c-40e7-989f-9436115b8f79',
        operation_id: '14391d4a-a52f-4b7d-bb1a-a4c4aa084fc5',
        recipient_id: '3024e69e-bf45-404d-b9d6-bace74fa80bc',
        item_received: 'Canned goods, utensils, bottled water, clothes, hygiene kits, and sleeping necessities.',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '74ca7819-24d7-4428-aa04-7c1d0dabe7cd',
        operation_id: '46c982eb-3f03-4204-9b49-04ba6d278a27',
        recipient_id: 'cc552f92-b352-4f5d-9cfb-163fae500285c',
        item_received: 'Canned goods, utensils, bottled water, clothes, hygiene kits, and sleeping necessities.',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '05255a00-f6f5-43b9-b8c8-8297fbed16e6',
        operation_id: '46c982eb-3f03-4204-9b49-04ba6d278a27',
        recipient_id: 'c1d7f036-1ee7-487c-8615-08335f3ea79b',
        item_received: 'Canned goods, utensils, bottled water, clothes, hygiene kits, and sleeping necessities.',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: 'b6951fae-c50b-4920-9e05-09b56d0a1fce',
        operation_id: '46c982eb-3f03-4204-9b49-04ba6d278a27',
        recipient_id: 'c5d93987-8334-4ca7-8065-9dc9e1a96124',
        item_received: 'Canned goods, utensils, bottled water, clothes, hygiene kits, and sleeping necessities.',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Relief_Recipients', null, {});
  }
};
