'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    
    await queryInterface.bulkInsert('Businesses', [
      {
        id: '420cbd46-b065-4b67-8dc2-a4d6bdcb3f3b',
        owner_id: '64bf1148-37f3-415e-80f2-6d400c1f458a',
        name: "Tita's Tapsi",
        house_number: '30',
        street: 'Rosal Street',
        barangay: 'Hehi 2',
        municipality: 'Ila',
        province: 'Region 18',
        contact_number: '9340059485',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Businesses', null, {});
  }
};
