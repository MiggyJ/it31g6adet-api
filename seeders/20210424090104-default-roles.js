'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('roles', [
      {
        id: '07eb9503-b297-4613-8939-39cd9b573cbb',
        name: 'User',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '8b36fc44-a338-4cf2-995e-4d4375c1369c',
        name: 'Admin',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '482fc1ad-0973-4f0f-9746-0a5591677296',
        name: 'Staff',
        created_at: new Date(),
        updated_at: new Date()
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Roles', null, {});

  }
};
