'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

     await queryInterface.bulkInsert('Ordinances', [{
      id: '8d38fa9e-acb6-4d6a-950b-79ff1c18fc96',
      created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      author: '423a6af8-7396-41a3-98d3-e133da71c784',
      year: new Date('2019-12-02').getFullYear(),
      ordinance_number: 'ORD-001',
      title: "Restricting Minors to Loiter in Public Places Between 10:00 PM to 5 AM Within the Municipal Jurisdiction of Teacher's Village",
      details: "A rise in the number of minors loitering in public places between 10:00 pm and 5:00 am has been observed within the municipal jurisdiction of Teacher's Village. If this kind of loitering remains unchecked, these minors shall conditionally be exposed to a number of social ills and ultimately become helpless victims of unscrupulous individual who are out to exploit their innocence. Hence this piece of legislation.",
      date_effective: new Date('2020-01-01'),
      status: 'Active',
      file: 'ORD-001.pdf',
      created_at: new Date(),
      updated_at: new Date()
     },
     {
      id: '34e46b04-c34f-48cd-a5f3-5f87e4c74f35',
      created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      author: '423a6af8-7396-41a3-98d3-e133da71c784',
      year: new Date('2020-09-12').getFullYear(),
      ordinance_number: 'ORD-002',
      title: "Establishing the Persons with Disability Affairs Office in Teacher's Village, Quezon City and Appropriating Funds Thereof",
      details: "There shall be established a Persons with Disability Affairs Office (PDAO) which shall be attached to the Barangay Health Center and the Caloocan City Health Department. The office shall be manned by sufficient number of employees to ensure delivery of services to the sector. Preference must be given to qualified persons with disabilities. There shall be no discrimination against a qualified disabled person by means of disability in regard to job application procedures; the hiring, promotion, or discharge of employees, employees compensation, job training; and other terms and conditions of employment.",
      date_effective: new Date('2020-10-11'),
      status: 'Active',
      file: 'ORD-002.pdf',
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      id: '7954b562-ffc9-4732-adb5-7561428d8130',
      created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      author: '423a6af8-7396-41a3-98d3-e133da71c784',
      year: new Date('2021-02-30').getFullYear(),
      ordinance_number: 'ORD-003',
      title: "Aruga at Kalinga ng mga Bata sa Barangay in Quezon City and Providing Funds Thereof",
      details: "Based on studies showing that children who grew up in institution and orphanages have a higher incidence of emotional disorders, dependency traits, and anti—social behaviour personality disorganization, it is a thrust of present social development work to promote the assignment of foster homes that will provide parental care to abused and disadvantaged children below eighteen (18) years of age. More families are encouraged to take in these children as the intangible benefits of foster care, like love; understanding and emotional support can only be harnessed in a family environment.",
      date_effective: new Date('2021-03-30'),
      status: 'Active',
      file: 'ORD-003.pdf',
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      id: 'c73c5e51-0862-4e65-b91f-163ca39c440c',
      created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      author: '423a6af8-7396-41a3-98d3-e133da71c784',
      year: new Date('2021-05-01').getFullYear(),
      ordinance_number: 'ORD-004',
      title: "An Order Placing the City of Quezon under Modified General Community Quarantine",
      details: "Modified General Community Quarantine (MGCQ) - view of the recommendation of the National LATF and the City Inter-agency Task Force, from 12:01 A.M. of June 1, 2021 to 11:59 P.M. of June 15, 2021. the entire City of Quezon is placed under Modified General Community Quarantine which refers to the transition phase between GCQ and New Normal, when the temporary measures are relaxed and become less necessary: limiting movement and transportation, the regulation of operating industries, and the presence of uniformed personnel to enforce community quarantine protocols.",
      date_effective: new Date('2021-06-01'),
      status: 'Inactive',
      file: 'ORD-004.pdf',
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      id: 'f8e1cbab-1152-4cf1-b74a-3c4c0026ac18',
      created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      author: '423a6af8-7396-41a3-98d3-e133da71c784',
      year: new Date('2021-06-30').getFullYear(),
      ordinance_number: 'ORD-005',
      title: "An Ordinance Enacting the Ecological Solid Waste Management Ordinance of the Municipality of Quezon City",
      details: "The Municipality of Quezon City, acting with the knowledge that the protection and the safeguarding of the health, welfare, safety and well-being of its constituents is one of its primary duties and functions and in line with its commitment to serve public interest in a proper and suitable manner and in pursuance with its continuing beautification, cleanliness and satisfaction drive, hereby declares as a matter of policy to keep and maintain at all times the Municipality of Quezon City as a 'SOLID WASTE FREE AREA'.",
      date_effective: new Date('2021-07-21'),
      status: 'Inactive',
      file: 'ORD-005.pdf',
      created_at: new Date(),
      updated_at: new Date()
    }
  ])
},

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Ordinances', null, {});
  }
};
