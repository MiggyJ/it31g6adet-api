'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('Witnesses', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Witnesses', [
      {
        id: '20c8396f-bcb2-4777-9412-790e9136df27',
        summon_id: '78e90b0e-bb0d-47eb-bcb0-80d599f29155',
        resident_id: '994384c5-208b-4f43-bfe2-e74efad380f5',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '66fe541b-a494-4e3b-b80a-429f9495f041',
        summon_id: '9daf76f4-d8d2-409d-8f9a-b56d65f8e35a',
        resident_id: '994384c5-208b-4f43-bfe2-e74efad380f5',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        id: '7c6d2999-973b-4df3-a64f-e90056598ed5',
        summon_id: 'a4d86b46-fb91-46dc-a0f2-adfb61c39beb',
        resident_id: '994384c5-208b-4f43-bfe2-e74efad380f5',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date()
      },
  ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Witnesses', null, {});
  }
};
