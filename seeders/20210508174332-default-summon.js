'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Summons', [
    {
      id: '78e90b0e-bb0d-47eb-bcb0-80d599f29155',
      complaint_id: '3c11f9b6-970e-4d79-a4a8-6e435904b53e',
      summon_number: 1,
      summon_date: new Date('01-20-21'),
      summon_time: '10:00',
      duration: '2',
      mediator_id: 'bb7b7049-a305-488b-b4f2-ce4496ac4803',
      details: 'Meeting happened, adjourned.',
      status: 'Adjourned',
      created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      id: '9daf76f4-d8d2-409d-8f9a-b56d65f8e35a',
      complaint_id: 'f34f7e38-b0ce-4be1-936d-1a94e5b87270',
      summon_number: 1,
      summon_date: new Date('01-20-21'),
      summon_time: '13:00',
      duration: '1',
      mediator_id: 'bb7b7049-a305-488b-b4f2-ce4496ac4803',
      details: 'Meeting happened, adjourned.',
      status: 'Adjourned',
      created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      created_at: new Date(),
      updated_at: new Date()
    },
    {
      id: 'a4d86b46-fb91-46dc-a0f2-adfb61c39beb',
      complaint_id: 'e2cc4334-577f-4796-bf41-dbdaba7a94f5',
      summon_number: 1,
      summon_date: new Date('02-03-21'),
      summon_time: '10:00',
      duration: '2',
      mediator_id: 'bb7b7049-a305-488b-b4f2-ce4496ac4803',
      details: 'Meeting happened, adjourned.',
      status: 'Adjourned',
      created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
      created_at: new Date(),
      updated_at: new Date()
    }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Summons', null, {});
  }
};
