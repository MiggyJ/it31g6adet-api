'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Relief_Operations', [
      {
        id: '7b7d07ef-5699-4f7e-b73e-c290c2241f0f',
        sponsor: 'A&B Philippines',
        details: 'A&B Philippines is a starting non-profit organization that aims to transform communities towards holistic development through technology.',
        operation_date: new Date('2021-4-20'),
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        status: 'Inactive',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '08d7c4e5-7c4d-407c-bfc2-15ea8fee955b',
        sponsor: 'Akbayanihan Foundation',
        details: "Akbayanihan is a non-profit organization rooted around the principles of service delivery with the aim of ensuring sustainable livelihood.",
        operation_date: new Date('2021-5-05'),
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        status: 'Inactive',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '0a30c27f-5ad5-43d2-98d1-b02cfd40b586',
        sponsor: 'Abot-Kamay PH',
        details: "Abot-Kamay is a youth-led charity program that focuses on bridging the gap between people who are in need and people who have the heart to help.",
        operation_date: new Date('2021-09-30'),
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        id: '14391d4a-a52f-4b7d-bb1a-a4c4aa084fc5',
        sponsor: 'Ambagan PH',
        details: 'Ambagan PH, a network of Filipino volunteers, is conducting immediate relief operations to help affected communities. The group accepts in-kind donations such as canned goods, utensils, bottled water, clothes, hygiene kits, and sleeping necessities. ',
        operation_date: new Date('2021-10-10'),
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      },      
      {
        id: '46c982eb-3f03-4204-9b49-04ba6d278a27',
        sponsor: 'Philippine Red Cross',
        details: 'The Philippine Red Cross is accepting cash and in-kind donations for typhoon-hit urban communities around Metro Manila.',
        operation_date: new Date('2021-11-01'),
        created_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        updated_by: '1f2915d1-95f9-430f-99a8-28bc34ec0600',
        status: 'Active',
        created_at: new Date(),
        updated_at: new Date(),
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Relief_Operations', null, {});
  }
};
