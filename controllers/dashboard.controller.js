const {
    Blotter,
    Business,
    Certificate,
    Complaint,
    Official,
    Ordinance,
    Resident,
    Relief_Operation,
    Role,
    Summon,
    User
} = require('../models')
const { Op } = require('sequelize')

const userDashboard = async (req, res) => {
    try {
        let data = {}
        data.certificates = await Certificate.count({
            where: {
                owner_id: req.user.resident_id,
                status: {
                    [Op.not]: 'Request'
                }
            }
        })
    
        data.blotters = await Blotter.count({
            where: {
                [Op.or]: [
                    { defendant_id: req.user.resident_id },
                    { complainant_id: req.user.resident_id }
                ]
            }
        })
    
        data.complaints = await Complaint.count({
            where: {
                [Op.or]: [
                    { defendant_id: req.user.resident_id },
                    { complainant_id: req.user.resident_id }
                ]
            }
        })
    
        let summons = await Summon.findAll({
            attributes: ['id'],
            include: [
                {
                    model: Complaint,
                    as: 'summon_complaint',
                    where: {
                        [Op.or]: [
                            { defendant_id: req.user.resident_id },
                            { complainant_id: req.user.resident_id }
                        ]
                    }
                }
            ]
        })
    
        data.summons = summons.length
    
        data.upcoming_summons = await Summon.findAll({
            attributes: ['summon_date', 'summon_time'],
            where: {
                [Op.or]: [
                    {
                        summon_date: {
                        [Op.gt]: new Date()
                        }
                    },
                    {
                        summon_date: new Date()
                    }
                ]
            },
            include: [
                {
                    model: Complaint,
                    as: 'summon_complaint',
                    where: {
                        [Op.or]: [
                            { defendant_id: req.user.resident_id },
                            { complainant_id: req.user.resident_id }
                        ]
                    },
                    attributes: ['id']
                },
                {
                    model: Official,
                    as: 'mediator',
                    attributes: ['id'],
                    include: {
                        model: Resident,
                        as: 'profile'
                    }
                }
            ],
            order: [
                ['summon_date', 'ASC'],
                ['summon_time', 'ASC']
            ]
        })
    
        data.certificate_list = await Certificate.findAll({
            where: {
                owner_id: req.user.resident_id,
                status: 'Approved'
            },
            attributes: ['type', 'file']
        })
    
        res.send({
            error: false,
            data
        })
    } catch (err) {
        console.log(err)
        res.send({
            error: true,
            message: 'Oop! Something went wrong.'
        })
    }
}

const staffDashboard = async (req, res) => {
    try {
        let data = {}
        data.certificates = await Certificate.count({
            where: {
                status: 'Approved'
            }
        })
    
        data.blotters = await Blotter.count()
    
        let complaints = await Complaint.findAll()
        data.complaints = complaints.length
    
        data.summons = await Summon.count()

        let residents = await Resident.findAll()
        data.residents = residents.length

        data.relief_operations = await Relief_Operation.count({
            where: {
                operation_date: {
                    [Op.lt]: new Date()
                }
            }
        })

        data.businesses = await Business.count()

        data.ordinances = await Ordinance.count()

        data.complaints_unresolved = complaints.filter(el=>el.status == 'Unresolved').length

        data.complaints_resolved = complaints.filter(el => el.status == 'Resolved').length
        
        data.male = residents.filter(el => el.gender == 'Male').length
        
        data.female = residents.filter(el => el.gender == 'Female').length

        data.nonbinary = residents.filter(el=>el.gender == 'Non-binary').length
    
        res.send({
            error: false,
            data
        })
    } catch (err) {
        console.log(err)
        res.send({
            error: true,
            message: 'Oop! Something went wrong.'
        })
    }
}

const adminDashboard = async (req, res) => {
    try {
        let data = {}
        let users = await User.findAll({include: ['role'], order: [['created_at', 'DESC']]})

        data.users = users.length

        data.residents = await Resident.count()

        data.staff = users.filter(el => el.role.name == 'Staff').length
        
        data.admin = users.filter(el => el.role.name == 'Admin').length

        let monthsData = [0,0,0,0,0,0,0,0,0,0,0,0]
        let sortedData = [0,0,0,0,0,0,0,0,0,0,0,0]
        let sortedLabel = [0,0,0,0,0,0,0,0,0,0,0,0]

        users.forEach(el => {
            let month = new Date(el.created_at).getMonth()
            monthsData[month]++
        })
        // console.log(monthsData)
        
        let currentMonth = new Date().getMonth()

        monthsData.forEach((el, index) => {
            if (index === currentMonth){
                sortedData[11] = el
                sortedLabel[11] = index
            }
            else {
                if (index > currentMonth){
                    sortedData[index - currentMonth] = el
                    sortedLabel[index - currentMonth - 1] = index
                } else {
                    sortedData[11 - (currentMonth - index)]
                    if (index == 0)
                        sortedLabel[11 - currentMonth] = index
                    else
                        sortedLabel[11 - (currentMonth - index)] = index
                }
            }
        })

        data.chartData = sortedData
        data.chartLabel = sortedLabel

        res.send({
            error: false,
            data
        })
    } catch (err) {
        console.log(err)
        res.send({
            error: true,
            message: 'Oops! Something went wrong.'
        })
    }
}



module.exports = {
    userDashboard,
    staffDashboard,
    adminDashboard
}