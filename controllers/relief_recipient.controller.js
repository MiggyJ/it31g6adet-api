const  { Relief_Recipient, Resident } = require('../models')
const datatables = require('sequelize-datatable')

const findOneReliefRecipient = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Relief_Recipient.findOne(
            {
                where: { id },
                include: [
                    {
                        model: Resident,
                        as: 'recipient_profile',
                    }
                ]
             })
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const datatableReliefRecipient = (req, res) => {
    let operation_id = req.query.operation_id
    datatables(Relief_Recipient, req.query, {
        where: {status: 'Active', operation_id},
        include: [
            {
                model: Resident,
                as: 'recipient_profile',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const updateReliefRecipient = async (req, res) => {
    let id = req.params.id
    try {
        let user = await Relief_Recipient.findOne({ where: { id } })
        if (user) {
            await Relief_Recipient.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'Relief Recipient updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Relief Recipient not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const deleteReliefRecipient  = async (req, res) => {
    let id = req.params.id
    try {
        
        let user = await Relief_Recipient.findOne({ where: { id } })
        if (user) {
            await Relief_Recipient.update({ status: 'Inactive' }, { where: { id } })
            res.send({
                error: false,
                message: 'Relief Recipient is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'Relief Recipient not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const createReliefRecipient  = async (req, res) => {
    try {
        let user = await Relief_Recipient.create(req.body)
        res.send({
            error: false,
            message: 'Relief Recipient created successfully!',
            data: user
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}


module.exports = {
    findOneReliefRecipient,
    datatableReliefRecipient,
    updateReliefRecipient,
    deleteReliefRecipient,
    createReliefRecipient
}