const { Role } = require('../models')

const findAll = async (req, res) => {
    try {
        let data = await Role.findAll()
        res.send({
            error: false,
            data,
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.message ? err.message : err.errors.map(el => el.message)
        })
    }
}

module.exports = {
    findAll
}