const  { Summon, Resident, Complaint, Official, Witness } = require('../models')
const datatables = require('sequelize-datatable')
const { Op }=require('sequelize')
//FindOne Complaint ID
//Find Complaint ID, show the complaint information
//done
const findOneSummon = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Summon.findOne(
            {
                where: { id },
                include: [
                    {
                        model: Complaint,
                        as: 'summon_complaint',
                        include: [
                            {
                                model: Resident,
                                as: 'complaint_by'
                            },
                            {
                                model: Resident,
                                as: 'complaint_defendant'
                            }
                        ]
                    },
                    {
                        model:Official,
                        as: 'mediator',
                        include: [
                            {
                                model: Resident,
                                as: 'profile'
                            } 
                         ]
                    },
                    {
                        model: Witness,
                        as: 'witness',
                        include: [
                            {
                                model: Resident,
                                as: 'resident_profile'
                            } 
                         ]
                    }
                ]
            })
        
        if (req.user.role.name == 'User') {
            if (
                data.summon_complaint.complainant_id != req.user.resident_id &&
                data.summon_complaint.defendant_id != req.user.resident_id
            ) {
                return res.send({
                    error: true,
                    message: 'You do not have permission to view this.'
                })
            }
        }
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors ? err.errors.map(el => el.message):err
        })
    }
}


//datatable as Both Complainant and Defendant
const datatableSummon = (req, res) => {
    datatables(Summon, req.query, {
        include: [
            {
                model: Official,
                as: 'mediator',
                include: [
                    {
                        model: Resident,
                        as: 'profile'
                    } 
                 ]
            },
            {
                model: Complaint,
                as: 'summon_complaint',
                include: [
                    {
                        model: Resident,
                        as: 'complaint_by'
                    },
                    {
                        model: Resident,
                        as: 'complaint_defendant'
                    }
                ]
            }
            
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//datatable summon
const datatableResidentSummon = (req, res) => {
    datatables(Summon, req.query, {
        include: [
            {
                model: Official,
                as: 'mediator',
                include: {
                    model: Resident,
                    as: 'profile'
                }
            },
            {
                model: Complaint,
                as: 'summon_complaint',
                include: [ 
                    {
                        model: Resident,
                        as: 'complaint_by',
                    },
                    {
                        model: Resident,
                        as: 'complaint_defendant',
                    }
                ],
                where:{
                    [Op.or]:[
                        {complainant_id: req.user.resident_id},
                        {defendant_id: req.user.resident_id}
                    ]
                }
            }
        ],
        order: [
            ['summon_date', 'DESC'],
            ['summon_time', 'DESC']
        ]

    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//update a Summon
const updateSummon = async (req, res) => {
    let id = req.params.id
    try {
        let summon = await Summon.findOne({ where: { id } })
        if (summon) {
            req.body.updated_by = req.user.id
            req.body.summon_date = new Date(req.body.summon_date)
            await Summon.update(req.body, { where: { id } })
            if (req.body.status == 'Concluded'){
                await Complaint.update(
                    { status: 'Resolved' },
                    { where: { id: summon.complaint_id } }
                )
            } else {
                await Complaint.update(
                    { status: 'Unresolved' },
                    { where: { id: summon.complaint_id } }
                )
            }
            res.send({
                error: false,
                message: 'Summon updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Summon not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

//soft delete
//summon id status = inactive
//db data not deleted.
const deleteSummon = async (req, res) => {
    let id = req.params.id
    try {
        
        let summon = await Summon.findOne({ where: { id } })
        if (summon) {
        req.body.updated_by = req.user.id
        await Summon.update({ status: 'Inactive' }, { where: { id } })
            res.send({
                error: false,
                message: 'Summon is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'Summon not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

//create a Summon
//done
const createSummon = async (req, res) => {
    try {
        let current = await Summon.findOne({
            where: {
                complaint_id: req.body.complaint_id,
                status: 'New'
            }
        })
        if (current == null) {
            let count = await Summon.count({
                where: {
                    complaint_id: req.body.complaint_id,
                }
            })

            let mediator = await Official.findOne({
                where: {
                    [Op.and]:[
                        {
                            term_start: {
                                [Op.lt]: new Date()
                            }
                        },
                        {
                            term_end: {
                                [Op.gt]: new Date()
                            }    
                        }
                    ],
                    position: 'Barangay Chairman'
                }
            }) 

            req.body.summon_number = count + 1
            req.body.mediator_id = mediator.id
            req.body.created_by = req.body.updated_by = req.user.id
            req.body.summon_date = new Date(req.body.summon_date)
            let summon = await Summon.create(req.body)
            res.send({
                error: false,
                message: 'Summon created successfully!',
                data: summon
            })
        } else {
            res.send({
                error: true,
                message: 'The last summon for this complaint is still unfulfilled.'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}


module.exports = {
    findOneSummon,
    datatableSummon,
    datatableResidentSummon,
    updateSummon,
    deleteSummon,
    createSummon
}