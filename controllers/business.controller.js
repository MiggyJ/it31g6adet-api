const  { Business, Resident } = require('../models')
const datatables = require('sequelize-datatable')

//FindOne Business ID
//Find Business ID
//done
const findOneBusiness = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Business.findOne(
            {
                where: { id },
                include: [
                    {
                        model: Resident,
                        as: 'business_owner',
                    }
                ]
            }
        )
        if (req.user.role.name == 'User') {
            if (data.owner_id != req.user.resident_id) {
                return res.send({
                    error: true,
                    message: 'You do not have permission to view this.'
                })
            }
        }
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors ? err.errors.map(el => el.message):err
        })
    }
}


//datatable for Staff
const datatableStaff = (req, res) => {
    datatables(Business, req.query, {
        where: {
            status: 'Active'
        },
        include: [
            {
                model: Resident,
                as: 'business_owner',
            }
        ]
        
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const datatableStaffInactive = (req, res) => {
    datatables(Business, req.query, {
        where: {
            status: 'Inactive'
        },
        include: [
            {
                model: Resident,
                as: 'business_owner',
            }
        ]
        
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//datatable for Owner
const datatableOwner = (req, res) => {
    datatables(Business, req.query, {
        where: {
            owner_id: req.user.resident_id,
            status: 'Active'
        },
        include: [
            {
                model: Resident,
                as: 'business_owner',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const datatableOwnerInactive = (req, res) => {
    datatables(Business, req.query, {
        where: {
            owner_id: req.user.resident_id,
            status: 'Inactive'
        },
        include: [
            {
                model: Resident,
                as: 'business_owner',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//update a Blotter
const updateBusiness = async (req, res) => {
    let id = req.params.id
    try {
        let business = await Business.findOne({ where: { id } })
        if (business) {
            await Business.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'Business updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Business not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

//soft delete
//Blotter id status = inactive
//db data not deleted.
const deleteBusiness = async (req, res) => {
    let id = req.params.id
    try {
        
        let business = await Business.findOne({ where: { id } })
        if (business) {
            await Business.update({ status: 'Inactive' }, { where: { id } })
            res.send({
                error: false,
                message: 'Business is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'Business not found'
            })
        }
    } catch (err) {
        console.log(err)
        res.send({
            error: true,
            message: err.errors.map(e=>e.message)
        })
    }
}

const restoreBusiness = async (req, res) => {
    let id = req.params.id
    try {
        
        let business = await Business.findOne({ where: { id } })
        if (business) {
            await Business.update({ status: 'Active' }, { where: { id } })
            res.send({
                error: false,
                message: 'Business is now restored.'
            })
        } else {
            res.send({
                error: true,
                message: 'Business not found'
            })
        }
    } catch (err) {
        console.log(err)
        res.send({
            error: true,
            message: err.errors.map(e=>e.message)
        })
    }
}

//create a Blotter
//done
const createBusiness = async (req, res) => {
    try {
        if (req.body.owner_id == null)
            req.body.owner_id = req.user.resident_id
        let business = await Business.create(req.body)
        res.send({
            error: false,
            message: 'Business created successfully!',
            data: business
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}


module.exports = {
    findOneBusiness,
    datatableStaff,
    datatableStaffInactive,
    datatableOwner,
    datatableOwnerInactive,
    updateBusiness,
    deleteBusiness,
    restoreBusiness,
    createBusiness
}