const  { Complaint, Official, Resident, Summon } = require('../models')
const { Op } = require('sequelize')
const datatables = require('sequelize-datatable')

//FindOne Complaint ID
//Find Complaint ID, show the complaint information
const findOneComplaint = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Complaint.findOne(
            {
                where: { id },
                include: [
                    {
                        model: Resident,
                        as: 'complaint_by',
                    },
                    {
                        model:Resident,
                        as: 'complaint_defendant',
                    },
                    {
                        model: Summon,
                        as: 'meetings',
                        include: {
                            model:Official,
                            as: 'mediator',
                            include: [
                                {
                                    model: Resident,
                                    as: 'profile'
                                } 
                             ]
                        }
                    }
                ]
            })
            if (req.user.role.name == 'User') {
                if (
                    data.complainant_id != req.user.resident_id &&
                    data.defendant_id != req.user.resident_id
                ) {
                    return res.send({
                        error: true,
                        message: 'You do not have permission to view this.'
                    })
                }
            }
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors ? err.errors.map(el => el.message):err
        })
    }
}

//find all datatables
const findAllComplaint = async (req, res) => {
    let data = await Complaint.findAll({
        where: {
            status: 'Unresolved'            
        }
    })
    res.send(data)
}



//datatable as Both Complainant and Defendant / isActive = true
const datatableBothComplaint = (req, res) => {
    datatables(Complaint, req.query, {
        where: {
            isActive: true
        },
        include: [
            {
                model: Resident,
                as: 'complaint_by',
            },
            {
                model:Resident,
                as: 'complaint_defendant',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}


//complaint = datatable for complainant / isActive = true
const datatableComplainant = (req, res) => {
    datatables(Complaint, req.query, {
        where: {
            complainant_id: req.user.resident_id,
            isActive: true
        },
        include: [
            {
                model: Resident,
                as: 'complaint_defendant',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//complaint = datatable for defendant / isActive = true
const datatableDefendant = (req, res) => {
    datatables(Complaint, req.query, {
        where: {
            defendant_id: req.user.resident_id,
            isActive: true
        },
        include: [
            {
                model: Resident,
                as: 'complaint_by',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//isActive both complainant and defendant = false
const inactiveDatatable = (req, res) => {
    datatables(Complaint, req.query, {
    where: {
        isActive: false
    },
        include: [
            {
                model: Resident,
                as: 'complaint_by',
            },
            {
                model:Resident,
                as: 'complaint_defendant',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//isActive complainant = false
const inactiveComplainant = (req, res) => {
    datatables(Complaint, req.query, {
        where: {
            complainant_id: req.user.resident_id,
            isActive: false
        },
        include: [
            {
                model: Resident,
                as: 'complaint_defendant',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//isActive defendant = false
const inactiveDefendant = (req, res) => {
    datatables(Complaint, req.query, {
        where: {
            defendant_id: req.user.resident_id,
            isActive: false
        },
        include: [
            {
                model: Resident,
                as: 'complaint_by',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//update a Complaint
const updateComplaint = async (req, res) => {
    let id = req.params.id
    try {
        req.body.updated_by = req.user.id
        let complaint = await Complaint.findOne({ where: { id } })
        if (complaint) {
            req.body.complaint_date = new Date(req.body.complaint_date)
            await Complaint.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'Complaint updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Complaint not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

//soft delete
//complaint id status = inactive
//db data not deleted.
const deleteComplaint = async (req, res) => {
    let id = req.params.id
    try {
        req.body.updated_by = req.user.id
        let complaint = await Complaint.findOne({ where: { id } })
        if (complaint) {
            await Complaint.update({ isActive: false }, { where: { id } })
            res.send({
                error: false,
                message: 'Complaint is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'Complaint not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

//restore
const restoreComplaint = async (req, res) => {
    let id = req.params.id
    try {
        req.body.updated_by = req.user.id
        let complaint = await Complaint.findOne({ where: { id } })
        if (complaint) {
            await Complaint.update({ isActive: true }, { where: { id } })
            res.send({
                error: false,
                message: 'Complaint is now restored.'
            })
        } else {
            res.send({
                error: true,
                message: 'Complaint not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

//create a complaint
//done
const createComplaint = async (req, res) => {
    try {

        let count = await Complaint.count({
            where: {
                complaint_number: {
                    [Op.like]: `COMPLAINT-${new Date().getFullYear()}_%`
                }
            }
        })

        req.body.complaint_number = `COMPLAINT-${new Date().getFullYear()}_${100+count+1}`
        req.body.created_by = req.body.updated_by = req.user.id
        req.body.complaint_date = new Date(req.body.complaint_date)

        let complaint = await Complaint.create(req.body)
        res.send({
            error: false,
            message: 'Complaint created successfully!',
            data: complaint
        })
    } catch (err) {
        console.log(err)
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}


module.exports = {
    findOneComplaint,
    findAllComplaint,
    datatableBothComplaint,
    datatableComplainant,
    datatableDefendant,
    inactiveDatatable,
    inactiveComplainant,
    inactiveDefendant,
    updateComplaint,
    deleteComplaint,
    restoreComplaint,
    createComplaint
}