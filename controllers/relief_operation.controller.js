const  { Relief_Operation, Relief_Recipient, Resident } = require('../models')
const datatables = require('sequelize-datatable')

const findOneReliefOperation = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Relief_Operation.findOne(
            {
                where: { id },
                include: [
                    {
                        model: Relief_Recipient,
                        as: 'recipients',
                        include: [
                            {
                                model: Resident,
                                as: 'recipient_profile',
                            }
                        ]
                    }
                ]
             })
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const datatableActiveReliefOperation  = (req, res) => {
    datatables(Relief_Operation, req.query, {
        where: {
            status: 'Active'
        }
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}


const datatableInactiveReliefOperation  = (req, res) => {
    datatables(Relief_Operation, req.query, {
        where: {
            status: 'Inactive'
        }
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const updateReliefOperation  = async (req, res) => {
    let id = req.params.id
    try {
        let reliefOps = await Relief_Operation.findOne({ where: { id } })
        if (reliefOps) {
            req.body.updated_by = req.user.id
            req.body.operation_date = new Date(req.body.operation_date)
            await Relief_Operation.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'Relief Operation updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Relief Operation not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const deleteReliefOperation  = async (req, res) => {
    let id = req.params.id
    try {
        
        req.body.updated_by = req.user.id
        let reliefOps = await Relief_Operation.findOne({ where: { id } })
        if (reliefOps) {
            await Relief_Operation.update({ status: 'Inactive' }, { where: { id } })
            res.send({
                error: false,
                message: 'Relief Operation is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'Relief Operation not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const restoreReliefOperation = async (req, res) => {
    let id = req.params.id
    try {
        
        req.body.updated_by = req.user.id
        let reliefOps= await Relief_Operation.findOne({ where: { id } })
        if (reliefOps) {
            await Relief_Operation.update({ status: 'Active' }, { where: { id } })
            res.send({
                error: false,
                message: 'Relief Operation is now restored.'
            })
        } else {
            res.send({
                error: true,
                message: 'Relief Operation not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const createReliefOperation  = async (req, res) => {
    try {
        req.body.created_by = req.body.updated_by = req.user.id
        req.body.operation_date = new Date(req.body.operation_date)
        let reliefOps = await Relief_Operation.create(req.body)
        res.send({
            error: false,
            message: 'Relief Operation created successfully!',
            data: reliefOps
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}


module.exports = {
    findOneReliefOperation,
    datatableActiveReliefOperation,
    datatableInactiveReliefOperation,
    updateReliefOperation,
    deleteReliefOperation,
    createReliefOperation,
    restoreReliefOperation
}