const { User, Role, Resident } = require('../models')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')


const login = async (req, res) => {
    let user = await User.findOne({
        include: [
            {
                model: Role,
                as: 'role'
            }  
        ],
        where: {
            email: req.body.email,
            status: 'Active'
        }
    }) || null

    if (user) {
        let match = await bcrypt.compare(req.body.password, user.password)
        if (match) {
            let token = await jwt.sign(JSON.stringify(user), process.env.JWT_SECRET)
            res.cookie('token', token, { httpOnly: true })
            res.cookie('role', user.role.name)
            return res.send({
                error: false,
                message: 'You are now logged in!'
            })
        }
    }
    res.send({
        error: true,
        message: 'Email and Password combination not found.'
    })
}

const logout = async (req, res) => {
    if (req.cookies.token) {
        res.cookie('token', '', { expires: new Date() })
        res.send({
            error: false,
            message: 'You are now logged out.'
        })
    } else {
        res.send({
            error: true,
            message: 'You are logged out.'
        })
    }
}

const register = async (req, res) => {
    try {
        let {
            first_name,
            last_name,
            current_street,
            birth_date,
            email,
            password,
            confirm_password
        } = req.body
        let data = await Resident.findOne({
            where: {
                first_name,
                last_name,
                birth_date: new Date(birth_date),
                current_street,
                status: 'Active'
            }
        })
        if (data) {
            if (password != confirm_password){
                res.send({
                    error: true,
                    message: ['Passwords must match']
                })
                return
            }

            password = await bcrypt.hash(password, 10)
            let user = await User.create({
                email,
                password,
                resident_id: data.id
            })
            res.send({
                error: false,
                message: 'User registered successfully.',
                user
            })
        } else {
            res.send({
                error: true,
                message: ['Resident not found.']
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors ? err.errors.map(el => el.message) : err
        })
    }
}

module.exports = {
    login,
    logout,
    register
}