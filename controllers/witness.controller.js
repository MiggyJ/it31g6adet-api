const  { Summon, Resident, Witness } = require('../models')
const datatables = require('sequelize-datatable')

//FindOne Witness ID
//Find Witness ID, show the complaint information
//done
const findOneWitness = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Witness.findOne(
            {
                where: { id },
                include: [
                    {
                        model: Summon,
                        as: 'summon_attended',
                    },
                    {
                        model:Resident,
                        as: 'resident_profile',
                    },
                ]
             })
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors ? err.errors.map(el => el.message):err
        })
    }
}


//datatable as witness
const datatableWitness = (req, res) => {
    datatables(Witness, req.query, {
        where: {
            summon_id: req.query.summon_id,
            status: 'Active'
        },
        include:['resident_profile']
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//update a Summon
const updateWitness = async (req, res) => {
    let id = req.params.id
    try {
        let witness = await Witness.findOne({ where: { id } })
        if (witness) {
            await Witness.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'Witness updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Witness not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

//soft delete
//witness id status = inactive
//db data not deleted.
const deleteWitness = async (req, res) => {
    let id = req.params.id
    try {
        
        let witness = await Witness.findOne({ where: { id } })
        if (witness) {
            await witness.destroy()
            res.send({
                error: false,
                message: 'Witness is now deleted.'
            })
        } else {
            res.send({
                error: true,
                message: 'Witness not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

//create a witness
//done
const createWitness = async (req, res) => {
    try {
        let witness = await Witness.create(req.body)
        res.send({
            error: false,
            message: 'Witness added successfully!',
            data: witness
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}


module.exports = {
    findOneWitness,
    datatableWitness,
    updateWitness,
    deleteWitness,
    createWitness
}