const  { Resident } = require('../models')
const datatables = require('sequelize-datatable')

const findOneResident = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Resident.findOne({ where: { id } })
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const findAllResident = async (req,res) => {
    let data = await Resident.findAll({ where: { status: 'Active' } })
    res.send(data)
}

const datatableActiveResident = (req, res) => {
    datatables(Resident, req.query, {
        where: {
            status: 'Active'
        }
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const datatableInactiveResident = (req, res) => {
    datatables(Resident, req.query, {
        where: {
            status: 'Inactive'
        }
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const updateResident = async (req, res) => {
    let id = req.params.id
    try {
        if (req.body.benefits === undefined)
            req.body.benefits = []
        else if (typeof req.body.benefits != 'array')
            req.body.benefits = [req.body.benefits]
        let user = await Resident.findOne({ where: { id } })
        if (user) {
            await Resident.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'Resident updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Resident not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const restoreResident = async (req, res) => {
    let id = req.params.id
    try {
        
        let user = await Resident.findOne({ where: { id } })
        if (user) {
            user = await Resident.update({ status: 'Active' }, { where: { id } })
            res.send({
                error: false,
                message: 'Resident is now restored.'
            })
        } else {
            res.send({
                error: true,
                message: 'Resident not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const deleteResident = async (req, res) => {
    let id = req.params.id
    try {
        
        let user = await Resident.findOne({ where: { id } })
        if (user) {
            user = await Resident.update({ status: 'Inactive' }, { where: { id } })
            res.send({
                error: false,
                message: 'Resident is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'Resident not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const createResident = async (req, res) => {
    try {
        if (req.body.benefits === undefined)
            req.body.benefits = []
        else if (typeof req.body.benefits != 'array')
            req.body.benefits = [req.body.benefits]
        let user = await Resident.create(req.body)
        res.send({
            error: false,
            message: 'Resident created successfully!',
            data: user
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}


module.exports = {
    findOneResident,
    findAllResident,
    datatableActiveResident,
    datatableInactiveResident,
    updateResident,
    restoreResident,
    deleteResident,
    createResident
}