const { Official, Resident } = require('../models')
const { Op } = require('sequelize')
const datatables = require('sequelize-datatable')

const findOneOfficial = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Official.findOne(
            {
                where: { id },
                include: [
                    {
                        model: Resident,
                        as: 'profile',
                    }
                ]
            }
        )
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const findCurrentOfficial = async (req, res) => {
    try{
        let data = await Official.findAll({
            where: {
                status: 'Active',
                [Op.and]: [
                    {
                        term_start: {
                            [Op.lt]: new Date()
                        }
                    },
                    {
                        term_end: {
                            [Op.gt]: new Date()
                        }    
                    }
                ]
            },
            include: [
                {
                    model: Resident,
                    as: 'profile'
                }
            ]
        })

        res.send({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const datatableOfficial = (req, res) => {
    datatables(Official, req.query, {
        where: {
          status: 'Active'  
        },
        include: {
            model: Resident,
            as: 'profile'
        }  
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const datatableCurrentOfficial = (req, res) => {
    datatables(Official, req.query, {
        where: {
            status: 'Active',
            [Op.and]: [
                {
                    term_start: {
                        [Op.lt]: new Date()
                    }
                },
                {
                    term_end: {
                        [Op.gt]: new Date()
                    }    
                }
            ]
        },
        include: ['profile']
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const datatableInactiveOfficial = (req, res) => {
    datatables(Official, req.query, {
        where: {
            status: 'Inactive'
        },
        include: {
            model: Resident,
            as: 'profile'
      }  
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const updateOfficial = async (req, res) => {
    let id = req.params.id
    try {
        let data = await Official.findOne({ where: { id } })
        if (data) {
            req.body.term_start = new Date(req.body.term_start)
            req.body.term_end = new Date(req.body.term_end)
            await Official.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'Official updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Official not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const restoreOfficial = async (req, res) => {
    let id = req.params.id
    try {
        
        let data = await Official.findOne({ where: { id } })
        if (data) {
            await Official.update({ status: 'Active' }, { where: { id } })
            res.send({
                error: false,
                message: 'Official is now restored.'
            })
        } else {
            res.send({
                error: true,
                message: 'Official not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const deleteOfficial = async (req, res) => {
    let id = req.params.id
    try {
        
        let data = await Official.findOne({ where: { id } })
        if (data) {
            await Official.update({ status: 'Inactive' }, { where: { id } })
            res.send({
                error: false,
                message: 'Official is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'Official not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const createOfficial = async (req, res) => {
    try {
        req.body.term_start = new Date(req.body.term_start)
        req.body.term_end = new Date(req.body.term_end)
        let user = await Official.create(req.body)
        res.send({
            error: false,
            message: 'Official created successfully!',
            data: user
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}


module.exports = {
    findOneOfficial,
    findCurrentOfficial,
    datatableOfficial,
    datatableCurrentOfficial,
    datatableInactiveOfficial,
    updateOfficial,
    restoreOfficial,
    deleteOfficial,
    createOfficial
}