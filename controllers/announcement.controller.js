const  { Announcement } = require('../models')
const datatables = require('sequelize-datatable')

const findOneAnnouncement= async (req, res) => {
    try {
        let id = req.params.id
        let data = await Announcement.findOne({ where: { id } })
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const latestAnnouncement = async (req, res) => {
    try {
        let data = await Announcement.findAll({
            where: {
                status: 'Active',
            },
            limit: 3,
            order: [['created_at', 'desc']]
        })
    
        res.send({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: [err.message]
        })
    }
}

const datatableActiveAnnouncement = (req, res) => {
    datatables(Announcement, req.query, {
        where: {
            status: 'Active'
        }
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const datatableInactiveAnnouncement = (req, res) => {
    datatables(Announcement, req.query, {
        where: {
            status: 'Inactive'
        }
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const paginateAnnouncement = async (req, res) => {
    try {
        let limit = parseInt(req.query._limit)
        let page = parseInt(req.query._page)

        let data = await Announcement.findAll({
            limit,
            offset: (limit * page) - limit,
            order: [
                ['created_at', 'DESC']
            ]
        })

        if (data.length) {
            res.send({
                error: false,
                data
            })
        } else {
            res.send({
                error: false,
                data
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const updateAnnouncement = async (req, res) => {
    let id = req.params.id
    try {
        if (req.file)
            req.body.file = req.file.filename
        req.body.updated_by = req.user.id
        let ann = await Announcement.findOne({ where: { id } })
        if (ann) {
            await Announcement.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'Announcement updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Announcement not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const deleteAnnouncement = async (req, res) => {
    let id = req.params.id
    try {
        
        let ann = await Announcement.findOne({ where: { id } })
        if (ann) {
            req.body.updated_by = req.user.id
            await Announcement.update({ status: 'Inactive' }, { where: { id } })
            res.send({
                error: false,
                message: 'Announcement is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'Announcement not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const restoreAnnouncement = async (req, res) => {
    let id = req.params.id
    try {
        
        req.body.updated_by = req.user.id
        let ann= await Announcement.findOne({ where: { id } })
        if (ann) {
            await Announcement.update({ status: 'Active' }, { where: { id } })
            res.send({
                error: false,
                message: 'Announcement is now restored.'
            })
        } else {
            res.send({
                error: true,
                message: 'Announcement not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}


const createAnnouncement = async (req, res) => {
    req.body.created_by = req.body.updated_by = req.user.id
    req.body.file = req.file != undefined ? req.file.filename : "";
    try {
        let ann = await Announcement.create(req.body)
        res.send({
            error: false,
            message: 'Announcement created successfully!',
            data: ann
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}


module.exports = {
    datatableActiveAnnouncement,
    datatableInactiveAnnouncement,
    findOneAnnouncement,
    latestAnnouncement,
    paginateAnnouncement,
    updateAnnouncement,
    deleteAnnouncement,
    restoreAnnouncement,
    createAnnouncement
}