const { Ordinance, Resident } = require('../models')
const datatables = require('sequelize-datatable')

const findOneOrdinance= async (req, res) => {
    try {
        let id = req.params.id
        let data = await Ordinance.findOne({
            where: { id },
            include: ['ordinance_author']
        })
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const paginateOrdinance = async (req, res) => {
    try {
        let limit = parseInt(req.query._limit)
        let page = parseInt(req.query._page)

        let data = await Ordinance.findAll({
            include: ['ordinance_author'],
            limit,
            offset: (limit * page) - limit,
            order: [
                ['year', 'DESC'],
                ['ordinance_number', 'DESC']
            ]
        })

        if (data.length) {
            res.send({
                error: false,
                data
            })
        } else {
            res.send({
                error: false,
                data
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const datatableOrdinance = (req, res) => {
    datatables(Ordinance, req.query, {
        order: ['ordinance_number'],
        include: ['ordinance_author']
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const updateOrdinance = async (req, res) => {
    let id = req.params.id
    try {
        let user = await Ordinance.findOne({ where: { id } })
        if (user) {
            if(req.file)
                req.body.file = req.file.filename
            req.body.updated_by = req.user.id
            req.body.date_effective = new Date(req.body.date_effective)
            await Ordinance.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'Ordinance updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Ordinance not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const deleteOrdinance = async (req, res) => {
    let id = req.params.id
    try {
        req.body.updated_by = req.user.id
        let ann = await Ordinance.findOne({ where: { id } })
        if (ann) {
            await Ordinance.update({ status: 'Inactive' }, { where: { id } })
            res.send({
                error: false,
                message: 'Ordinance is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'Ordinance not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const createOrdinance = async (req, res) => {
    req.body.created_by = req.body.updated_by = req.user.id
    req.body.file = req.file.filename
    req.body.year = new Date().getFullYear()
    req.body.date_effective = new Date(req.body.date_effective)

    try {
        let ann = await Ordinance.create(req.body)
        res.send({
            error: false,
            message: 'Ordinance created successfully!',
            data: ann
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}


module.exports = {
    findOneOrdinance,
    paginateOrdinance,
    datatableOrdinance,
    updateOrdinance,
    deleteOrdinance,
    createOrdinance
}