const { User, Resident, Role } = require('../models')
const bcrypt = require('bcrypt')
const datatables = require('sequelize-datatable')

const getProfile = async (req, res) => {
    try {
        let data = await User.findOne({
            where: {
                id: req.user.id
            },
            include: 'profile'
        })

        res.send({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const findOneUser = async (req, res) => {
    try {
        let id = req.params.id
        let data = await User.findOne({ where: { id }, include: ['profile', 'role'] })
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const datatableUser = (req, res) => {
    datatables(User, req.query, {
        where: {
            status: 'Active'
        },
        include: [
            {
                model: Resident,
                as: 'profile'
            },
            'role'
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const datatableInactiveUser = (req, res) => {
    datatables(User, req.query, {
        where: {
            status: 'Inactive'
        },
        include: [
            {
                model: Resident,
                as: 'profile'
            },
            'role'
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const updateUser = async (req, res) => {
    let id = req.params.id
    try {
        let user = await User.findOne({ where: { id } })
        if (user) {
            await User.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'User updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'User not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const restoreUser = async (req, res) => {
    let id = req.params.id
    try {
        
        let user = await User.findOne({ where: { id } })
        if (user) {
            await User.update({ status: 'Active' }, { where: { id } })
            res.send({
                error: false,
                message: 'User is now restored.'
            })
        } else {
            res.send({
                error: true,
                message: 'User not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const deleteUser = async (req, res) => {
    let id = req.params.id
    try {
        
        let user = await User.findOne({ where: { id } })
        if (user) {
            await User.update({ status: 'Inactive' }, { where: { id } })
            res.send({
                error: false,
                message: 'User is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'User not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const createUser = async (req, res) => {
    try {
            
        req.body.password = await bcrypt.hash(req.body.password, 10)
        let user = await User.create(req.body)
        res.send({
            error: false,
            message: 'User registered successfully.',
            user
        })

    } catch (err) {
        res.send({
            error: true,
            message: err.errors ? err.errors.map(el => el.message) : err
        })
    }
}


module.exports = {
    getProfile,
    findOneUser,
    datatableUser,
    datatableInactiveUser,
    updateUser,
    restoreUser,
    deleteUser,
    createUser
}