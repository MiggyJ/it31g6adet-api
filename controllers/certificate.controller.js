const { Certificate, Official, Resident, User } = require('../models')
const { Op } = require('sequelize')
const datatables = require('sequelize-datatable')
const qrcode = require('qrcode')
const ejs = require('ejs')
const pdf = require('html-pdf')
const fs = require('fs')

const findOneCertificate = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Certificate.findOne({
            where: { id },
            include: [
                {
                    model: Resident,
                    as: 'certificate_owner'
                },
                {
                    model: User,
                    as: 'user_approved',
                    include: ['profile']
                }
            ]
        })
        if (req.user.role.name == 'User') {
            if (data.owner_id != req.user.resident_id) {
                return res.send({
                    error: true,
                    message: 'You do not have permission to view this.'
                })
            }
        }
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err. errors ? err.errors.map(el => el.message) : err
        })
    }
}

const datatableUser = (req, res) => {
    datatables(Certificate, req.query, {
        where: {
            owner_id: req.user.resident_id,
            [Op.or]: [
                { status: 'Approved' },
                { status: 'Request' }
            ]
        }
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const datatableRequest = (req, res) => {
    datatables(Certificate, req.query, {
        where: {
            status: 'Request'
        },
        include: [
            {
                model: Resident,
                as: 'certificate_owner'
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const datatableReject = (req, res) => {
    datatables(Certificate, req.query, {
        where: {
            status: 'Rejected'
        },
        include: [
            {
                model: Resident,
                as: 'certificate_owner'
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const datatableAccept = (req, res) => {
    datatables(Certificate, req.query, {
        where: {
            status: 'Approved'
        },
        include: [
            {
                model: Resident,
                as: 'certificate_owner'
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

const approveCertificate = async (req, res) => {
    let id = req.params.id
    let certificate = await Certificate.findOne(
        {
            where: { id },
            include: [
                {
                    model: Resident,
                    as: 'certificate_owner'
                }
            ]
        }
    )

    if (certificate.status == 'Approved') {
        return res.send({
            error: true,
            message: 'This certificate is already approved.'
        })    
    }

    let type = certificate.type
    let temp_name = `${type}-${certificate.certificate_number}.pdf`

    let qrName = `qr-${certificate.certificate_number}.png`
    await qrcode.toFile(
        `./uploads/${qrName}`,
        `http://localhost:3000/uploads/certificates/${type}/${temp_name}`
    )

    function ordinal_suffix_of(i) {
        var j = i % 10,
            k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }

    const captain = await Official.findOne({
        where: {
            position: 'Barangay Chairman',
            status: 'Active',
            [Op.and]: [
                {
                    term_start: {
                        [Op.lt]: new Date()
                    }
                },
                {
                    term_end: {
                        [Op.gt]: new Date()
                    }    
                }
            ]
        },
        include: [
            {
                model: Resident,
                as: 'profile'
            }
        ]
    })

    let template

    if (certificate.type == 'Barangay') {
        template = await ejs.renderFile(
            './templates/cert_barangay.ejs',
            {
                name: certificate.certificate_owner.full_name,
                age: certificate.certificate_owner.age,
                civil_status: certificate.certificate_owner.civil_status,
                nationality: certificate.certificate_owner.nationality || 'Filipino',
                current_street: certificate.certificate_owner.current_street,
                day: ordinal_suffix_of(new Date().getDate()),
                month: new Date().toLocaleDateString('default', { month: 'long' }),
                year: new Date().getFullYear(),
                captain: captain.profile.full_name,
                file: `http://localhost:3000/uploads/${qrName}`
            }
        )
    } else if (certificate.type == 'Good Moral') {
        template = await ejs.renderFile(
            './templates/cert_goodmoral.ejs',
            {
                name: certificate.certificate_owner.full_name,
                age: certificate.certificate_owner.age,
                civil_status: certificate.certificate_owner.civil_status,
                nationality: certificate.certificate_owner.nationality || 'Filipino',
                gender: certificate.certificate_owner.gender == 'Male' ? 'He' : 'She',
                gender_small: certificate.certificate_owner.gender == 'Male' ? 'he' : 'she',
                day: ordinal_suffix_of(new Date().getDate()),
                month: new Date().toLocaleDateString('default', { month: 'long' }),
                year: new Date().getFullYear(),
                captain: captain.profile.full_name,
                file: `http://localhost:3000/uploads/${qrName}`
            }
        )
    } else if (certificate.type == 'Indigency') {
        template = await ejs.renderFile(
            './templates/cert_indigency.ejs',
            {
                name: certificate.certificate_owner.full_name,
                age: certificate.certificate_owner.age,
                civil_status: certificate.certificate_owner.civil_status,
                nationality: certificate.certificate_owner.nationality || 'Filipino',
                current_street: certificate.certificate_owner.current_street,
                day: ordinal_suffix_of(new Date().getDate()),
                month: new Date().toLocaleDateString('default', { month: 'long' }),
                year: new Date().getFullYear(),
                captain: captain.profile.full_name,
                file: `http://localhost:3000/uploads/${qrName}`
            }
        )
    } else if (certificate.type == 'Business') {
        template = await ejs.renderFile(
            './templates/cert_business.ejs',
            {
                owner: certificate.certificate_owner.full_name,
                permit_number: certificate.certificate_number,
                business_name: certificate.business_name,
                business_address: certificate.business_address,
                validity: new Date(new Date().setMonth(new Date().getMonth() + 6)).toLocaleString('default', {month: 'long', day: 'numeric', year: 'numeric'}),
                day: ordinal_suffix_of(new Date().getDate()),
                month: new Date().toLocaleDateString('default', { month: 'long' }),
                year: new Date().getFullYear(),
                captain: captain.profile.full_name,
                file: `http://localhost:3000/uploads/${qrName}`
            }
        )
    }
 
    pdf
        .create(template, {
            format: 'Letter',
        })
        .toFile(`./uploads/certificates/${type}/${temp_name}`, async (err, data) => {
            if (err) throw err
            fs.unlinkSync('./uploads/' + qrName)
            
            await Certificate.update(
                {
                    file: temp_name,
                    approved_by: req.user.id,
                    status: 'Approved',
                    date_approved: new Date(),
                    valid_until: new Date().setMonth(new Date().getMonth() + 6)
                },
                {
                    where: { id }
                }
            )

            res.send({
                error: false,
                message: 'Certificate approved.'
            })
        })
}

const rejectCertificate = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Certificate.findOne({ where: { id } }) || null
        if (data) {
            await Certificate.update({
                approved_by: req.user.id,
                status: 'Rejected',
            }, {
                where: {id}
            })
    
            res.send({
                error: false,
                message: 'Certificate rejected.'
            })
        } else {
            res.send({
                error: true,
                message: 'Certificate not found.'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}

const requestCertificate = async (req, res) => {
    try {
        if(!req.body.owner_id)
            req.body.owner_id = req.user.resident_id
        let request = await Certificate.findOne({
            where: {
                owner_id: req.body.owner_id,
                type: req.body.type,
                [Op.or]:{
                    valid_until: {
                        [Op.gt]: new Date()
                    },
                    status: 'Request'
                }
            }
        }) || null

        if (request) {
            return res.send({
                error: true,
                message: 'This request is already in process or is already valid.'
            })
        }
        let prefix
        switch (req.body.type) {
            case 'Barangay':
                prefix = new Date().getFullYear() + '-A'
                break
            case 'Business':
                prefix = new Date().getFullYear() + '-B'
                break
            case 'Good Moral':
                prefix = new Date().getFullYear() + '-C'
                break
            case 'Indigency':
                prefix = new Date().getFullYear() + '-D'
                break
        
            default:
                prefix = new Date().getFullYear() + '-E'
                break
        }

        let count = await Certificate.count(
            {
                where: {
                    type: req.body.type,
                    certificate_number: {
                        [Op.like]: `${prefix}%`
                    }
                }
            }
        )

        
        req.body.certificate_number = prefix + (10000 + count + 1)
        let data = await Certificate.create(req.body)
        res.send({
            error: false,
            message: 'Certificate request sent.',
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors ? err.errors.map(el => el.message) : err
        })
    }
}


module.exports = {
    findOneCertificate,
    datatableUser,
    datatableRequest,
    datatableAccept,
    datatableReject,
    approveCertificate,
    rejectCertificate,
    requestCertificate
}