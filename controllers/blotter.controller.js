const { sequelize, Blotter, Official, Resident } = require('../models')
const { Op } = require('sequelize')
const datatables = require('sequelize-datatable')

//FindOne Blotter ID
//Find Blotter ID, show the complaint information
const findOneBlotter = async (req, res) => {
    try {
        let id = req.params.id
        let data = await Blotter.findOne(
            {
                where: { id },
                include: [
                    {
                        model: Resident,
                        as: 'blotter_complainant',
                    },
                    {
                        model:Resident,
                        as: 'blotter_defendant',
                    }
                ]
            })
        if (req.user.role.name == 'User') {
            if (
                data.complainant_id != req.user.resident_id &&
                data.defendant_id != req.user.resident_id
            ) {
                return res.send({
                    error: true,
                    message: 'You do not have permission to view this.'
                })
            }
        }
        res.json({
            error: false,
            data
        })
    } catch (err) {
        res.send({
            error: true,
            message: err.errors ? err.errors.map(el => el.message):err
        })
    }
}


//datatable as Both Complainant and Defendant for blotter
const datatableBothBlotter = (req, res) => {
    datatables(Blotter, req.query, {
        where: {
            status: 'Active'
        },
        include: [
            {
                model: Resident,
                as: 'blotter_complainant',
            },
            {
                model:Resident,
                as: 'blotter_defendant',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//blotter = datatable for blotter complainant
const datatableComplainant = (req, res) => {
    datatables(Blotter, req.query, {
        where: {
            complainant_id: req.user.resident_id,
            status: 'Active'
        },
        include: [
            {
                model: Resident,
                as: 'blotter_defendant',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//blotter = datatable for blotter defendant
const datatableDefendant = (req, res) => {
    datatables(Blotter, req.query, {
        where: {
            defendant_id: req.user.resident_id,
            status: 'Active'
        },
        include: [
            {
                model: Resident,
                as: 'blotter_complainant',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//datatable as Both Complainant and Defendant for blotter
const inactiveDatatable = (req, res) => {
    datatables(Blotter, req.query, {
        where: {
            status: 'Inactive'
        },
        include: [
            {
                model: Resident,
                as: 'blotter_complainant',
            },
            {
                model:Resident,
                as: 'blotter_defendant',
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//blotter = datatable for blotter complainant
const inactiveComplainant = (req, res) => {
    datatables(Blotter, req.query, {
        where: {
            complainant_id: req.user.resident_id,
            status: 'Inactive'
        },
        include: [
            {
                model: Resident,
                as: 'blotter_defendant',
                where: {id: req.user.resident_id}
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//blotter = datatable for blotter defendant
const inactiveDefendant = (req, res) => {
    datatables(Blotter, req.query, {
        where: {
            defendant_id: req.user.resident_id,
            status: 'Inactive'
        },
        include: [
            {
                model: Resident,
                as: 'blotter_complainant',
                where: {id: req.user.resident_id}
            }
        ]
    }).then((data) => {
        res.json(data)
    }).catch(err => console.log(err))
}

//update a Blotter
const updateBlotter = async (req, res) => {
    let id = req.params.id
    try {
        req.body.updated_by = req.user.id
        req.body.incidence_date = new Date(req.body.incidence_date)
        let blotter = await Blotter.findOne({ where: { id } })
        if (blotter) {
            await Blotter.update(req.body, { where: { id } })
            res.send({
                error: false,
                message: 'Blotter updated successfully!'
            })
        } else {
            res.send({
                error: true,
                message: 'Blotter not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

//soft delete
//Blotter id status = inactive
//db data not deleted.
const deleteBlotter = async (req, res) => {
    let id = req.params.id
    try {
        
        req.body.updated_by = req.user.id
        let blotter = await Blotter.findOne({ where: { id } })
        if (blotter) {
            await Blotter.update({ status: 'Inactive' }, { where: { id } })
            res.send({
                error: false,
                message: 'Blotter is now archived.'
            })
        } else {
            res.send({
                error: true,
                message: 'Blotter not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

const restoreBlotter = async (req, res) => {
    let id = req.params.id
    try {
        
        req.body.updated_by = req.user.id
        let blotter = await Blotter.findOne({ where: { id } })
        if (blotter) {
            await Blotter.update({ status: 'Active' }, { where: { id } })
            res.send({
                error: false,
                message: 'Blotter is now restored.'
            })
        } else {
            res.send({
                error: true,
                message: 'Blotter not found'
            })
        }
    } catch (err) {
        res.send({
            error: true,
            message: err.errors.map(el => el.message)
        })
    }
}

//create a Blotter
//done
const createBlotter = async (req, res) => {
    try {
        let count = await Blotter.count({
            where: 
                sequelize.where(
                    sequelize.fn('YEAR', sequelize.col('created_at')),
                    new Date().getFullYear()
                )
            
        })

        let mediator = await Official.findOne({
            where: {
                [Op.and]:[
                    {
                        term_start: {
                            [Op.lt]: new Date()
                        }
                    },
                    {
                        term_end: {
                            [Op.gt]: new Date()
                        }    
                    }
                ],
                position: 'Barangay Chairman'
            }
        }) 

        req.body.officer_id = mediator.id
        req.body.incidence_date = new Date(req.body.incidence_date)
        req.body.created_by = req.body.updated_by = req.user.id
        req.body.blotter_number = count++ > 9 ? new Date().getFullYear() + '-' + count :
            new Date().getFullYear() + '-0' + count
        let blotter = await Blotter.create(req.body)
        res.send({
            error: false,
            message: 'Blotter created successfully!',
            data: blotter
        })
    } catch (err) {
        res.send({
            error: true,
            message: err
        })
    }
}


module.exports = {
    findOneBlotter,
    datatableBothBlotter,
    datatableComplainant,
    datatableDefendant,
    inactiveDatatable,
    inactiveComplainant,
    inactiveDefendant,
    updateBlotter,
    deleteBlotter,
    restoreBlotter,
    createBlotter
}