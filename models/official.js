'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Official extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Resident, { foreignKey: 'resident_id', as: 'profile' })

      this.hasMany(models.Blotter, { foreignKey: 'officer_id', as: 'blotter_signed' })
      this.hasMany(models.Summon, {foreignKey: 'mediator_id', as: 'summon_mediated'})
    }
  };
  Official.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    resident_id: {
      type: DataTypes.UUID,
      allowNull: false,
      unique: {
        name: 'UQ_OFFICIAL_TERM',
        msg: 'This resident is already an official for the term given.'
      }
    },
    position: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    term_start: {
      type: DataTypes.DATEONLY,
      get(){
        return new Date(this.getDataValue('term_start')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      },
      allowNull: false,
      validate: {
        lessThanEnd(value) {
          if (new Date(value) > new Date(this.term_end))
            throw new Error('Start of term cannot be later than the end.')
        }
      }
    },
    term_end: {
      type: DataTypes.DATEONLY,
      get(){
        return new Date(this.getDataValue('term_end')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      },
      allowNull: false,
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'Active'
    },
    created_at : {
      type: DataTypes.DATE,
      get(){
        return new Date(this.getDataValue('created_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
    updated_at : {
      type: DataTypes.DATE,
      get(){
        return new Date(this.getDataValue('updated_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
}, {
    sequelize,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'Official',
  });
  return Official;
};