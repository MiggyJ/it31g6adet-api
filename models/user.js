'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Announcement, {foreignKey: 'created_by', as: 'announcements_created'})
      this.hasMany(models.Announcement, { foreignKey: 'updated_by', as: 'announcements_updated' })
      this.hasMany(models.Ordinance, { foreignKey: 'created_by', as: 'ordinance_created'})
      this.hasMany(models.Ordinance, { foreignKey: 'updated_by', as: 'ordinance_updated' })
      this.hasMany(models.Relief_Operation, { foreignKey: 'created_by', as: 'operation_created'})
      this.hasMany(models.Relief_Operation, { foreignKey: 'updated_by', as: 'operation_updated' })
      this.belongsTo(models.Role, { foreignKey: 'role_id', as: 'role' })
      this.belongsTo(models.Resident, {foreignKey: 'resident_id', as: 'profile'})
    }
  };
  User.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    resident_id: {
      type: DataTypes.UUID,
      unique: { msg: 'This resident already has an account.' },
      validate: {
        isNotAdmin(value) {
          if (this.role_id != '8b36fc44-a338-4cf2-995e-4d4375c1369c' &&
            (value == '' || value == null)
          )
            throw new Error('Resident Profile is required.')
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isEmail: {msg: 'Please enter a valid email.'}
      },
      unique: {msg: 'The email you entered is already registered.'}
    },
    password: DataTypes.STRING,
    role_id: {
      type: DataTypes.UUID,
      allowNull: false,
      defaultValue: '07eb9503-b297-4613-8939-39cd9b573cbb'
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'Active'
    },
    created_at : {
    type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('created_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
    updated_at : {
      type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('updated_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
}, {
    sequelize,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'User',
  });
  return User;
};