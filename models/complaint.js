'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Complaint extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Resident, {foreignKey: 'complainant_id', as: 'complaint_by'})
      this.belongsTo(models.Resident, { foreignKey: 'defendant_id', as: 'complaint_defendant' })
      this.belongsTo(models.User, { foreignKey: 'created_by', as: 'complaint_created_by' })
      this.belongsTo(models.User, { foreignKey: 'updated_by', as: 'complaint_updated_by' })

      this.hasMany(models.Summon, {foreignKey: 'complaint_id', as: 'meetings'})
    }
  };
  Complaint.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    complaint_number: {
      type: DataTypes.STRING,
      allowNull: false
    },
    complainant_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    defendant_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    complaint_date: {
      type: DataTypes.DATEONLY,
      get(){
        return new Date(this.getDataValue('complaint_date')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      },
      allowNull: false
    },
    details: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Unresolved',
      validate: {
        isIn: [['Unresolved', 'Resolved']]
      }
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.UUID,
      allowNull: false
    },
    created_at : {
      type: DataTypes.DATE,
      get(){
        return new Date(this.getDataValue('created_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
    updated_at : {
      type: DataTypes.DATE,
      get(){
        return new Date(this.getDataValue('updated_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
}, {
    sequelize,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'Complaint',
  });
  return Complaint;
};