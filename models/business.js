'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Business extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Resident, {foreignKey: 'owner_id', as: 'business_owner'})
    }
  };
  Business.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    owner_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    house_number: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Business house number address cannot be empty.'}
      }
    },
    street:{
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Business street address cannot be empty.'}
      }
    },
    barangay:{
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Business barangay address cannot be empty.'}
      }
    },
    municipality: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Business municipality address cannot be empty.'}
      }
    },
    province:{
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Business province address cannot be empty.'}
      }
    },
    full_address: {
      type: DataTypes.VIRTUAL,
      get() {
        if (
          this.get('house_number') != null &&
          this.get('street') != null &&
          this.get('barangay') != null &&
          this.get('municipality') != null &&
          this.get('province') != null
        ) {
          return (
            `${this.get('house_number')} ${this.get('street')}, ${this.get('barangay')}, ${this.get('municipality')}, ${this.get('province')}`
          )
        } else {
          return null
        }
      }
    },
    contact_number: {
      type: DataTypes.STRING,
      validate: {
        isFormatted(value) {
          if (value != '') {
            if (value.length != 10)
              throw new Error('Contact Number should be the 10 digits after +63.')
          }
        }
      }
    },
    email: {
      type: DataTypes.STRING,
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Active'
    },
    created_at : {
    type: DataTypes.DATE,
    get(){
      return new Date(this.getDataValue('created_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
    updated_at : {
      type: DataTypes.DATE,
      get(){
        return new Date(this.getDataValue('updated_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
}, {
    sequelize,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'Business',
  });
  return Business;
};