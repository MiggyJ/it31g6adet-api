'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Resident extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Blotter, {foreignKey: 'complainant_id', as: 'blotter_complaints'})
      this.hasMany(models.Blotter, { foreignKey: 'defendant_id', as: 'blotter_against' })
      this.hasMany(models.Complaint, { foreignKey: 'complainant_id', as: 'complaints_created' })
      this.hasMany(models.Complaint, { foreignKey: 'defendant_id', as: 'complaints_against' })
      this.hasMany(models.Business, { foreignKey: 'owner_id', as: 'businesses' })
      this.hasMany(models.Certificate, { foreignKey: 'owner_id', as: 'certificates' })
      this.hasMany(models.Official, { foreignKey: 'resident_id', as: 'official_activity' })
      this.hasMany(models.Relief_Recipient, { foreignKey: 'recipient_id', as: 'relief_received' })
      this.hasOne(models.User, { foreignKey: 'resident_id', as: 'user_credential' })
      this.hasMany(models.Witness, { foreignKey: 'resident_id', as: 'summon_witness' })
    }
  };
  Resident.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    first_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: {msg: 'First Name cannot be empty.'}
      }
    },
    middle_name: {
      type: DataTypes.STRING(50)
    },
    last_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Last Name cannot be empty.'}
      }
    },
    full_name: {
      type: DataTypes.VIRTUAL,
      get() {
        if (this.middle_name == null) {
          return `${this.first_name} ${this.last_name}`
        } else {
          return `${this.first_name} ${this.middle_name} ${this.last_name}`
        }
      }
    },
    gender: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Age cannot be empty.'}
      }
    },
    civil_status: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Civil Status cannot be empty.'}
      }
    },
    personal_contact_number: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [10]
      }
    },
    birth_date: {
      type: DataTypes.DATEONLY,
      get(){
        return new Date(this.getDataValue('birth_date')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      },
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Birth Date cannot be empty.'}
      },
    },
    age: {
      type: DataTypes.VIRTUAL,
      get() {
        return new Date().getFullYear() - new Date(this.birth_date).getFullYear()
      }
    },
    permanent_house_number: {
      type: DataTypes.STRING,
      validate: {
        isPermanentAddress(value) {
          if (
            value == '' &&
            (
              this.permanent_house_number != '' ||
              this.permanent_street != '' ||
              this.permanent_barangay != '' ||
              this.permanent_municipality != '' ||
              this.permanent_province != ''
            )
          ) {
            throw new Error('Permanent house number cannot be empty if other parts of permanent address has values.')
          }
        }
      }
    },
    permanent_street: {
      type: DataTypes.STRING,
      validate: {
        isPermanentAddress(value) {
          if (
            value == '' &&
            (
              this.permanent_house_number != '' ||
              this.permanent_street != '' ||
              this.permanent_barangay != '' ||
              this.permanent_municipality != '' ||
              this.permanent_province != ''
            )
          ) {
            throw new Error('Permanent street cannot be empty if other parts of permanent address has values.')
          }
        }
      }
    },
    permanent_barangay: {
      type: DataTypes.STRING,
      validate: {
        isPermanentAddress(value) {
          if (
            value == '' &&
            (
              this.permanent_house_number != '' ||
              this.permanent_street != '' ||
              this.permanent_barangay != '' ||
              this.permanent_municipality != '' ||
              this.permanent_province != ''
            )
          ) {
            throw new Error('Permanent barangay cannot be empty if other parts of permanent address has values.')
          }
        }
      }
    },
    permanent_municipality: {
      type: DataTypes.STRING,
      validate: {
        isPermanentAddress(value) {
          if (
            value == '' &&
            (
              this.permanent_house_number != '' ||
              this.permanent_street != '' ||
              this.permanent_barangay != '' ||
              this.permanent_municipality != '' ||
              this.permanent_province != ''
            )
          ) {
            throw new Error('Permanent municipality cannot be empty if other parts of permanent address has values.')
          }
        }
      }
    },
    permanent_province: {
      type: DataTypes.STRING,
      validate: {
        isPermanentAddress(value) {
          if (
            value == '' &&
            (
              this.permanent_house_number != '' ||
              this.permanent_street != '' ||
              this.permanent_barangay != '' ||
              this.permanent_municipality != '' ||
              this.permanent_province != ''
            )
          ) {
            throw new Error('Permanent province cannot be empty if other parts of permanent address has values.')
          }
        }
      }
    },
    permanent_address: {
      type: DataTypes.VIRTUAL,
      get() {
        if (
          this.get('permanent_house_number') != null &&
          this.get('permanent_street') != null &&
          this.get('permanent_barangay') != null &&
          this.get('permanent_municipality') != null &&
          this.get('permanent_province') != null
        ) {
          return(
            `${this.get('permanent_house_number')} ${this.get('permanent_street')}, ${this.get('permanent_barangay')}, ${this.get('permanent_municipality')}, ${this.get('permanent_province')}`
          )
        } else {
          return null
        }
      }
    },
    current_house_number: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Current house number cannot be empty.'}
      }
    },
    current_street: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Current street cannot be empty.'}
      },
      unique: {name: 'UQ_RESIDENT_PROFILE', msg: 'This resident is already in the database.'}
    },
    current_barangay: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Current barangay cannot be empty.'}
      }
    },
    current_municipality: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Current municipality cannot be empty.'}
      }
    },
    current_province: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'Current province cannot be empty.'}
      }
    },
    current_address: {
      type: DataTypes.VIRTUAL,
      get() {
        if (
          this.get('current_house_number') != null &&
          this.get('current_street') != null &&
          this.get('current_barangay') != null &&
          this.get('current_municipality') != null &&
          this.get('current_province') != null
        ) {
          return ( 
            `${this.get('current_house_number')} ${this.get('current_street')}, ${this.get('current_barangay')}, ${this.get('current_municipality')}, ${this.get('current_province')}`
          )
        } else {
          return null
        }
      }
    },
    benefits: {
      type: DataTypes.STRING,
      get() {
        const raw = this.getDataValue('benefits')
        if(raw != undefined)
          return raw.split(';')
      },
      set(val) {
        if(val.length)
          this.setDataValue('benefits', val.join(';'))
        else
          this.setDataValue('benefits', '')
      }
    },
    emergency_contact_person_1: {
      type: DataTypes.STRING,
      get() {
        let raw = this.getDataValue('emergency_contact_person_1')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    emergency_contact_number_1: {
      type: DataTypes.STRING,
      get() {
        let raw = this.getDataValue('emergency_contact_number_1')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    emergency_contact_person_2: {
      type: DataTypes.STRING,
      get() {
        let raw = this.getDataValue('emergency_contact_person_2')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    emergency_contact_number_2: {
      type: DataTypes.STRING,
      get() {
        let raw = this.getDataValue('emergency_contact_number_2')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    citizenship: {
      type: DataTypes.STRING,
      get() {
        let raw = this.getDataValue('citizenship')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    nationality: {
      type: DataTypes.STRING,
      get() {
        let raw = this.getDataValue('nationality')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    blood_type: {
      type: DataTypes.STRING,
      get() {
        let raw = this.getDataValue('blood_type')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    occupation: {
      type: DataTypes.STRING,
      get() {
        let raw = this.getDataValue('occupation')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    height: {
      type: DataTypes.INTEGER,
      get() {
        let raw = this.getDataValue('height')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    weight: {
      type: DataTypes.INTEGER,
      get() {
        let raw = this.getDataValue('weight')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    hair_color: {
      type: DataTypes.STRING,
      get() {
        let raw = this.getDataValue('hair_color')
        if (raw == '')
          return null
        else
          return raw
      }
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Active',
    },
    created_at : {
      type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('created_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
    updated_at : {
      type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('updated_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
}, {
    sequelize,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'Resident',
  });
  return Resident;
};