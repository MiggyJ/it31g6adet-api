'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Blotter extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Resident, { foreignKey: 'complainant_id', as: 'blotter_complainant' })
      this.belongsTo(models.Resident, { foreignKey: 'defendant_id', as: 'blotter_defendant' })
      this.belongsTo(models.Official, {foreignKey: 'officer_id', as: 'blotter_officer'})
    }
  };
  Blotter.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    blotter_number: {
      type: DataTypes.STRING,
      allowNull: false
    },
    complainant_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    defendant_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    reason: {
      type: DataTypes.STRING,
      allowNull: false
    },
    details: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    incidence_place: {
      type: DataTypes.STRING,
      allowNull: false
    },
    incidence_date: {
      type: DataTypes.DATEONLY,
      get(){
        return new Date(this.getDataValue('incidence_date')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      },
      allowNull: false
    },
    officer_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'Active'
    },
    created_at : {
      type: DataTypes.DATE,
      get(){
        return new Date(this.getDataValue('created_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
    updated_at : {
    type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('updated_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
}, {
    sequelize,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'Blotter',
  });
  return Blotter;
};