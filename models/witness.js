'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Witness extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Summon, { foreignKey: 'summon_id', as: 'summon_attended' })
      this.belongsTo(models.Resident, {foreignKey: 'resident_id', as: 'resident_profile'})
    }
  };
  Witness.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    summon_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    resident_id: {
      type: DataTypes.UUID,
      allowNull: false,
      unique: {name: 'UQ_SUMMON_WITNESS', msg: 'This resident has already been added in this summon.'}
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'Active'
    },
    created_at : {
    type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('created_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
    updated_at : {
      type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('updated_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
}, {
    sequelize,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'Witness',
  });
  return Witness;
};