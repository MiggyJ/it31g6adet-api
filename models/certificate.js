'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Certificate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Resident, {foreignKey: 'owner_id', as: 'certificate_owner'})
      this.belongsTo(models.User, {foreignKey: 'approved_by', as: 'user_approved'})
    }
  };
  Certificate.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    certificate_number: {
      type: DataTypes.STRING,
      allowNull: false
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false
    },
    owner_id:  {
      type: DataTypes.UUID,
      allowNull: false
    },
    business_name: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        requireName(value) {
          if ((value == '' || value === null) && this.type == 'Business')
            throw new Error('Name of business is required for business certificate.')
        }
      }
    },
    business_address: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        requireAddress(value) {
          if ((value == '' || value === null) && this.type == 'Business')
            throw new Error('Address of business is required for business certificate.')
        }
      }
    },
    approved_by:  {
      type: DataTypes.UUID,
    },
    date_approved:  {
      type: DataTypes.DATEONLY,
      get() {
        if (this.getDataValue('date_approved') == null)
          return null
        return new Date(this.getDataValue('date_approved')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      },
    },
    valid_until: {
      type: DataTypes.DATEONLY,
      get(){
        if (this.getDataValue('valid_until') == null)
          return null
        return new Date(this.getDataValue('valid_until')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      },
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'Request'
    },
    file: {
      type: DataTypes.STRING
    },
    created_at : {
      type: DataTypes.DATE,
      get(){
        return new Date(this.getDataValue('created_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
    updated_at : {
      type: DataTypes.DATE,
      get(){
        return new Date(this.getDataValue('updated_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
}, {
    sequelize,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'Certificate',
  });
  return Certificate;
};