'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Relief_Operation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {foreignKey: 'created_by', as: 'operation_created_by'})
      this.belongsTo(models.User, {foreignKey: 'updated_by', as: 'operation_updated_by'})
      
      this.hasMany(models.Relief_Recipient, {foreignKey: 'operation_id', as: 'recipients'})
      
    }
  };
  Relief_Operation.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    sponsor: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    details: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    operation_date: {
      type: DataTypes.DATEONLY,
      get(){
        return new Date(this.getDataValue('operation_date')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      },
      allowNull: false,
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'Active'
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.UUID,
      allowNull: false
    },
    created_at : {
      type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('created_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
    updated_at : {
      type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('updated_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
}, {
    sequelize,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'Relief_Operation',
  });
  return Relief_Operation;
};