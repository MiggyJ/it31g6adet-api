'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Summon extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Complaint, { foreignKey: 'complaint_id', as: 'summon_complaint' })
      this.belongsTo(models.Official, { foreignKey: 'mediator_id', as: 'mediator' })
      
      this.hasMany(models.Witness, {foreignKey: 'summon_id', as: 'witness'})
    }
  };
  Summon.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    complaint_id: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    summon_number: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    summon_date: {
      type: DataTypes.DATEONLY,
      get(){    
        return new Date(this.getDataValue('summon_date')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      },
      allowNull: false
    },
    summon_time: {
      type: DataTypes.TIME,
      allowNull: false
    },
    duration: {
      type: DataTypes.INTEGER,
    },
    mediator_id: {
      type: DataTypes.UUID,
    },
    details: {
      type: DataTypes.TEXT,
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'New',
      validate: {
        isIn: [['New', 'Adjourned', 'Concluded']]
      }
    },
    created_by: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    updated_by: {
      type: DataTypes.UUID,
      allowNull: false
    },
    created_at : {
      type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('created_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
    updated_at : {
      type: DataTypes.DATE,
      get(){
      return new Date(this.getDataValue('updated_at')).toLocaleDateString('default', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        })
      }
    },
}, {
    sequelize,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    modelName: 'Summon',
  });
  return Summon;
};