require('dotenv').config()
const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')

// Sequelize ORM
const { sequelize } = require('./models')

// Express App
const app = express()


// Middlewares
app.use(cors({origin: 'http://localhost', credentials: true}))
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cookieParser(process.env.COOKIE_SECRET))

// Routes
app.use('/uploads', express.static(path.join(__dirname, 'uploads')))

const userRoutes = require('./routes/user.routes')
const residentRoutes = require('./routes/resident.routes')
const authRoutes = require('./routes/auth.routes')
const officialRoutes = require('./routes/official.routes')
const blotterRoutes = require('./routes/blotter.routes')
const complaintRoutes = require('./routes/complaint.routes')
const certificateRoutes = require('./routes/certificate.routes')
const summonRoutes = require('./routes/summon.routes')
const witnessRoutes = require('./routes/witness.routes')
const announcementRoutes = require('./routes/announcement.routes')
const reliefOperationRoutes = require('./routes/relief_operation.routes')
const reliefRecipientRoutes = require('./routes/relief_recipient.routes')
const businessRoutes = require('./routes/business.routes')
const ordinanceRoutes = require('./routes/ordinance.routes')
const rolesRoutes = require('./routes/roles.routes')
const dashboardRoutes = require('./routes/dashboard.routes')

app.use('/api/user', userRoutes)
app.use('/api/resident', residentRoutes)
app.use('/api/auth', authRoutes)
app.use('/api/official', officialRoutes)
app.use('/api/complaint', complaintRoutes)
app.use('/api/blotter', blotterRoutes)
app.use('/api/certificate', certificateRoutes)
app.use('/api/summon', summonRoutes)
app.use('/api/witness', witnessRoutes)
app.use('/api/announcement', announcementRoutes)
app.use('/api/relief_operation', reliefOperationRoutes)
app.use('/api/relief_recipient', reliefRecipientRoutes)
app.use('/api/business', businessRoutes)
app.use('/api/ordinance', ordinanceRoutes)
app.use('/api/roles', rolesRoutes)
app.use('/api/dashboard', dashboardRoutes)

// Catch error routes
const unauthorized = require('./utils/unauthorized')

app.use(unauthorized)

// Start Application
app.listen(3000, async () => {
    await sequelize.authenticate()
})