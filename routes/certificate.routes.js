const router = require('express').Router()
const {
    findOneCertificate,
    datatableRequest,
    approveCertificate,
    requestCertificate,
    datatableUser,
    datatableAccept,
    datatableReject,
    rejectCertificate,
} = require('../controllers/certificate.controller')

const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

router.get('/datatablesUser', jwtMiddleware, datatableUser)
router.get('/datatablesRequest', jwtMiddleware, staffMiddleWare, datatableRequest)
router.get('/datatablesAccept', jwtMiddleware, staffMiddleWare, datatableAccept)
router.get('/datatablesReject', jwtMiddleware, staffMiddleWare, datatableReject)
router.get('/:id', jwtMiddleware, findOneCertificate)
router.put('/approve/:id?', jwtMiddleware, staffMiddleWare, approveCertificate)
router.post('/request', jwtMiddleware, requestCertificate)
router.delete('/reject/:id', jwtMiddleware, staffMiddleWare, rejectCertificate)


module.exports = router
