const router = require('express').Router()
const {
    findOneAnnouncement,
    datatableActiveAnnouncement,
    updateAnnouncement,
    deleteAnnouncement,
    restoreAnnouncement,
    createAnnouncement,
    latestAnnouncement,
    paginateAnnouncement,
    datatableInactiveAnnouncement
} = require('../controllers/announcement.controller')

const multer = require("multer");
const path = require("path");
const filter = require("../utils/fileHelper");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, "../uploads/announcements/"));
    },
  
    // By default, multer removes file extensions so let's add them back
    filename: function (req, file, cb) {
      cb(
        null,
        file.fieldname + "-" + Date.now() + path.extname(file.originalname)
      );
    },
});

  const uploadImage = (req, res, next) => {
    let upload = multer({
      storage: storage,
      fileFilter: filter
    }).single("file");
  
    upload(req, res, function (err) {
      // req.file contains information of uploaded file
      // req.body contains information of text fields, if there were any
  
      if (req.fileValidationError) {
        return res.status(500).send({
          error: true,
          data: [],
          message: [req.fileValidationError],
        });
      } else if (!req.file) {
        return res.status(500).send({
          error: true,
          data: [],
          message: ["Please select an image to upload"],
        });
      } else if (err instanceof multer.MulterError) {
        return res.status(500).send({
          error: true,
          data: [],
          message: [err],
        });
      } else if (err) {
        return res.status(500).send({
          error: true,
          data: [],
          message: [err],
        });
      }
  
      next();
    });
  };

const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

router.get('/datatablesActive', jwtMiddleware, staffMiddleWare, datatableActiveAnnouncement)
router.get('/datatablesInactive', jwtMiddleware, staffMiddleWare, datatableInactiveAnnouncement)
router.get('/paginate', paginateAnnouncement)
router.get('/latest', latestAnnouncement)
router.get('/:id', findOneAnnouncement)
router.post('/create',jwtMiddleware, staffMiddleWare,  uploadImage, createAnnouncement)
router.put('/:id', jwtMiddleware, staffMiddleWare, uploadImage, updateAnnouncement)
router.delete('/:id', jwtMiddleware, staffMiddleWare, deleteAnnouncement)
router.put('/restore/:id', jwtMiddleware, staffMiddleWare, restoreAnnouncement)

module.exports = router