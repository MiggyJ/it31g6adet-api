const router = require('express').Router()
const {
    findOneOrdinance,
    datatableOrdinance,
    updateOrdinance,
    deleteOrdinance,
    createOrdinance,
    paginateOrdinance
} = require('../controllers/ordinance.controller')

const multer = require("multer");
const path = require("path");
const filter = require("../utils/ordinanceHelper");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, "../uploads/ordinances/"));
    },
  
    // By default, multer removes file extensions so let's add them back
    filename: function (req, file, cb) {
      cb(
        null,
        file.originalname
      );
    },
  });


  const uploadFile = (req, res, next) => {
    let upload = multer({
      storage: storage,
      fileFilter: filter
    }).single("file");
  
    upload(req, res, function (err) {
      // req.file contains information of uploaded file
      // req.body contains information of text fields, if there were any
  
      if (req.fileValidationError) {
        return res.status(500).send({
          error: true,
          data: [],
          message: [req.fileValidationError],
        });
      } else if (err instanceof multer.MulterError) {
        return res.status(500).send({
          error: true,
          data: [],
          message: [err],
        });
      } else if (err) {
        return res.status(500).send({
          error: true,
          data: [],
          message: [err],
        });
      }
  
      next();
    });
  };
  

const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

router.get('/datatables',jwtMiddleware, staffMiddleWare, datatableOrdinance)
router.get('/paginate', paginateOrdinance)
router.get('/:id', findOneOrdinance)
router.put('/:id', jwtMiddleware, staffMiddleWare, uploadFile, updateOrdinance)
router.delete('/:id', jwtMiddleware, staffMiddleWare, deleteOrdinance)
router.post('/create',jwtMiddleware, staffMiddleWare, uploadFile, createOrdinance)

module.exports = router