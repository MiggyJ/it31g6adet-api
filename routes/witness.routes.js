const router = require('express').Router()
const {
    findOneWitness,
    datatableWitness,
    updateWitness,
    deleteWitness,
    createWitness
} = require('../controllers/witness.controller')

const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

//http://localhost:3000/api/witness
router.get('/datatables', jwtMiddleware, datatableWitness)
router.get('/:id', jwtMiddleware, staffMiddleWare, findOneWitness)
router.post('/create', jwtMiddleware, staffMiddleWare, createWitness)
router.put('/:id', jwtMiddleware, staffMiddleWare, updateWitness)
router.delete('/:id', jwtMiddleware, staffMiddleWare, deleteWitness)

module.exports = router