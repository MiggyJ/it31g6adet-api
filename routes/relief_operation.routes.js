const router = require('express').Router()
const {
    findOneReliefOperation,
    datatableActiveReliefOperation,
    updateReliefOperation,
    deleteReliefOperation,
    createReliefOperation,
    datatableInactiveReliefOperation,
    restoreReliefOperation
} = require('../controllers/relief_operation.controller')

const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

router.get('/datatablesActive', jwtMiddleware, staffMiddleWare, datatableActiveReliefOperation)
router.get('/datatablesInactive', jwtMiddleware, staffMiddleWare, datatableInactiveReliefOperation)
router.get('/:id',jwtMiddleware, staffMiddleWare, findOneReliefOperation)
router.post('/create',jwtMiddleware, staffMiddleWare, createReliefOperation)
router.put('/:id',jwtMiddleware, staffMiddleWare, updateReliefOperation)
router.delete('/:id',jwtMiddleware, staffMiddleWare, deleteReliefOperation)
router.put('/restore/:id', jwtMiddleware, staffMiddleWare, restoreReliefOperation)

module.exports = router