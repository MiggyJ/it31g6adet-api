const router = require('express').Router()
const {
    findOneBlotter,
    datatableBothBlotter,
    datatableComplainant,
    datatableDefendant,
    inactiveDatatable,
    inactiveComplainant,
    inactiveDefendant,
    updateBlotter,
    deleteBlotter,
    createBlotter,
    restoreBlotter
} = require('../controllers/blotter.controller')

const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

//http://localhost:3000/api/complaint
router.get('/datatables', jwtMiddleware, staffMiddleWare, datatableBothBlotter)
router.get('/datatablesComplainant', jwtMiddleware, datatableComplainant)
router.get('/datatablesDefendant', jwtMiddleware, datatableDefendant)
router.get('/inactivedatatables' , jwtMiddleware, staffMiddleWare, inactiveDatatable)
router.get('/inactivecomplainant' , jwtMiddleware , inactiveComplainant)
router.get('/inactivedefendant' , jwtMiddleware , inactiveDefendant)
router.get('/:id', jwtMiddleware, findOneBlotter)
router.post('/create', jwtMiddleware, staffMiddleWare, createBlotter)
router.put('/restore/:id', jwtMiddleware, staffMiddleWare, restoreBlotter)
router.put('/:id', jwtMiddleware, staffMiddleWare, updateBlotter)
router.delete('/:id', jwtMiddleware, staffMiddleWare, deleteBlotter)

module.exports = router