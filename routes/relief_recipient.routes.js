const router = require('express').Router()
const {
    findOneReliefRecipient,
    datatableReliefRecipient,
    updateReliefRecipient,
    deleteReliefRecipient,
    createReliefRecipient
} = require('../controllers/relief_recipient.controller')

const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

router.get('/datatables',jwtMiddleware, staffMiddleWare, datatableReliefRecipient)
router.get('/:id',jwtMiddleware, staffMiddleWare, findOneReliefRecipient)
router.post('/create',jwtMiddleware, staffMiddleWare, createReliefRecipient)
router.put('/:id',jwtMiddleware, staffMiddleWare, updateReliefRecipient)
router.delete('/:id',jwtMiddleware, staffMiddleWare, deleteReliefRecipient)

module.exports = router