const router = require('express').Router()
const { findAll } = require('../controllers/roles.controller')

router.get('/findAll', findAll)


module.exports = router