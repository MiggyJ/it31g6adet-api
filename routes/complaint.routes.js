const router = require('express').Router()
const {
    findOneComplaint,
    inactiveDatatable,
    inactiveComplainant,
    inactiveDefendant,
    datatableBothComplaint,
    datatableComplainant,
    datatableDefendant,
    updateComplaint,
    deleteComplaint,
    createComplaint,
    restoreComplaint,
    findAllComplaint
} = require('../controllers/complaint.controller')

const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

//http://localhost:3000/api/complaint
router.get('/datatables', jwtMiddleware, staffMiddleWare, datatableBothComplaint)
router.get('/datatablesComplainant', jwtMiddleware, datatableComplainant)
router.get('/datatablesDefendant', jwtMiddleware, datatableDefendant)
router.get('/findAll', jwtMiddleware, staffMiddleWare, findAllComplaint)
router.get('/inactivedatatables', jwtMiddleware, staffMiddleWare, inactiveDatatable)
router.get('/inactivecomplainant', jwtMiddleware, inactiveComplainant)
router.get('/inactivedefendant', jwtMiddleware, inactiveDefendant)
router.get('/:id', jwtMiddleware, findOneComplaint)
router.post('/create', jwtMiddleware, staffMiddleWare, createComplaint)
router.put('/restore/:id', jwtMiddleware, staffMiddleWare, restoreComplaint)
router.put('/:id', jwtMiddleware, staffMiddleWare, updateComplaint)
router.delete('/:id', jwtMiddleware, staffMiddleWare, deleteComplaint)

module.exports = router