const router = require('express').Router()
const {
    findOneResident,
    datatableActiveResident,
    updateResident,
    deleteResident,
    createResident,
    findAllResident,
    restoreResident,
    datatableInactiveResident
} = require('../controllers/resident.controller')

const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

router.get('/datatablesActive', jwtMiddleware, staffMiddleWare, datatableActiveResident)
router.get('/datatablesInactive', jwtMiddleware, staffMiddleWare, datatableInactiveResident)
router.get('/findAll', jwtMiddleware, findAllResident)
router.get('/:id', jwtMiddleware, staffMiddleWare, findOneResident)
router.post('/create', jwtMiddleware, staffMiddleWare, createResident)
router.put('/restore/:id', jwtMiddleware, staffMiddleWare, restoreResident)
router.put('/:id', jwtMiddleware, updateResident)
router.delete('/:id', jwtMiddleware, staffMiddleWare, deleteResident)


module.exports = router