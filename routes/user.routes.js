const router = require('express').Router()
const {
    findOneUser,
    datatableUser,
    updateUser,
    deleteUser,
    createUser,
    restoreUser,
    datatableInactiveUser,
    getProfile
} = require('../controllers/user.controller')
const jwtMiddleware = require('../utils/jwtMiddleware')
const adminMiddleware = require('../utils/adminMiddleware')

router.get('/profile', jwtMiddleware, getProfile)
router.get('/datatables', jwtMiddleware, adminMiddleware, datatableUser)
router.get('/datatablesInactive', jwtMiddleware, adminMiddleware, datatableInactiveUser)
router.get('/:id', jwtMiddleware, adminMiddleware, findOneUser)
router.post('/create', jwtMiddleware, adminMiddleware, createUser)
router.put('/restore/:id', jwtMiddleware, adminMiddleware, restoreUser)
router.put('/:id', jwtMiddleware, adminMiddleware, updateUser)
router.delete('/:id', jwtMiddleware, adminMiddleware, deleteUser)

module.exports = router