const router = require('express').Router()
const {
    findOneOfficial,
    findCurrentOfficial,
    datatableOfficial,
    updateOfficial,
    deleteOfficial,
    createOfficial,
    restoreOfficial,
    datatableCurrentOfficial,
    datatableInactiveOfficial,
} = require('../controllers/official.controller')
const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

router.get('/datatables', jwtMiddleware, staffMiddleWare, datatableOfficial)
router.get('/datatablesCurrent', jwtMiddleware, staffMiddleWare, datatableCurrentOfficial)
router.get('/datatablesInactive', jwtMiddleware, staffMiddleWare, datatableInactiveOfficial)
router.get('/current', findCurrentOfficial)
router.get('/:id', findOneOfficial)
router.post('/create', jwtMiddleware, staffMiddleWare, createOfficial)
router.put('/restore/:id', jwtMiddleware, staffMiddleWare, restoreOfficial)
router.put('/:id', jwtMiddleware, staffMiddleWare, updateOfficial)
router.delete('/:id', jwtMiddleware, staffMiddleWare, deleteOfficial)

module.exports = router