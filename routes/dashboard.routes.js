const router = require('express').Router()
const {
    userDashboard,
    staffDashboard,
    adminDashboard
} = require('../controllers/dashboard.controller')

const jwtMiddleware = require('../utils/jwtMiddleware')

router.get('/user', jwtMiddleware, userDashboard)
router.get('/staff', jwtMiddleware, staffDashboard)
router.get('/admin', jwtMiddleware, adminDashboard)


module.exports = router