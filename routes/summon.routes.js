const router = require('express').Router()
const {
    findOneSummon,
    datatableSummon,
    datatableResidentSummon,
    updateSummon,
    deleteSummon,
    createSummon
} = require('../controllers/summon.controller')

const jwtMiddleware = require('../utils/jwtMiddleware')
const staffMiddleWare = require('../utils/staffMiddleWare')

//http://localhost:3000/api/summon
router.get('/datatables', jwtMiddleware, staffMiddleWare, datatableSummon)
router.get('/datatableResident', jwtMiddleware, datatableResidentSummon)
router.get('/:id', jwtMiddleware, findOneSummon)
router.post('/create', jwtMiddleware, staffMiddleWare, createSummon)
router.put('/:id', jwtMiddleware, staffMiddleWare, updateSummon)
router.delete('/:id', jwtMiddleware, staffMiddleWare, deleteSummon)


module.exports = router