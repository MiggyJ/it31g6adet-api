const router = require('express').Router()
const {
    findOneBusiness,
    datatableStaff,
    datatableOwner,
    updateBusiness,
    deleteBusiness,
    createBusiness,
    datatableStaffInactive,
    datatableOwnerInactive,
    restoreBusiness
} = require('../controllers/business.controller')

const jwtMiddleware = require('../utils/jwtMiddleware')

router.get('/datatables', datatableStaff)
router.get('/datatablesInactive', datatableStaffInactive)
router.get('/datatableOwner', jwtMiddleware, datatableOwner)
router.get('/datatableOwnerInactive', jwtMiddleware, datatableOwnerInactive)
router.get('/:id', jwtMiddleware, findOneBusiness)
router.post('/create', jwtMiddleware, createBusiness)
router.put('/:id', jwtMiddleware, updateBusiness)
router.put('/restore/:id', jwtMiddleware, restoreBusiness)
router.delete('/:id', jwtMiddleware, deleteBusiness)


module.exports = router