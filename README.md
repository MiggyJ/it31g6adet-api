# BSIT 3-1 ADET GROUP 6 
Repository for: Barangay Management System API
# Members
* Rica Bristol
* Lance Hubahib
* Stephen Laberinto
* Joseph Lamina
* Dominic Lomeda
* Jairus Montante
# To Run
* Prerequisites:
    - XAMPP (MySQL) is installed.
    - nodemon is installed globally.
    - sequelize-cli is installed globally.
1. Install dependencies with yarn or npm.
    - ```yarn install```
    - ```npm run i```
2. Copy .env.example and make it a .env file containing your database configuration.
3. Make the sequelize database.
    - ```sequelize db:create```
    - ```sequelize db:migrate```
    - ```sequelize db:seed:all```
4. Start the server
    - ```yarn dev```

# Migrations and Seeders
* To redo migrations, run ```yarn migrate```
* To redo seeders, run ```yarn seed```